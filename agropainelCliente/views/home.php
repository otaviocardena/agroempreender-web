<style>
<?php include '../css/home_style.css'; ?>
</style>
<div class="container-fluid">
    <div class="card">
        <div class="row" style="margin: 10px 20px 20px 20px;">
            <div class="col-xl-7 col-lg-7">
                <h2>Últimas Novidades</h2>
                <div class="slideshow-container" style="max-width: 650px;">

                    <div class="mySlides ">
                        <img src="img/Rectangle 5.png" style="width:100%;">
                    </div>
                    <div class="mySlides ">
                        <img src="img/Rectangle 5.png" style="width:100%">
                    </div>
                    <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a> -->
                    <div style="text-align: center;">
                        <span class="dot" onclick="currentSlide(1)"></span>
                        <span class="dot" onclick="currentSlide(2)"></span>
                    </div>
                </div>
                <h2>Seus Planos</h2>
                <div id="accordion">
                    <div class="row" style="flex-wrap: nowrap;padding: 20px;">
                        <div class="cardPlano col-lx-3 col-lg-3" style="margin-right: 10px;">
                            <img src="img/teeth.png" alt="">
                            <h4 class="titleOfPlan">Consórcio Odontológico</h4>
                            <p class="descOfPlan">A solução perfeita para você, em busca do sorriso perfeito.</p>
                        </div>
                        <div class="cardPlano col-lx-3 col-lg-3" style="margin-right: 10px;">
                            <img src="img/car.png" alt="">
                            <h4 class="titleOfPlan">Consórcio Auto</h4>
                            <p class="descOfPlan">O plano ideal para garantir a segurança do seu veículo.</p>
                        </div>
                        <div class="cardPlano col-lx-3 col-lg-3" style="margin-right: 10px;">
                            <img src="img/heart.png" alt="">
                            <h4 class="titleOfPlan">Consórcio Estético</h4>
                            <p class="descOfPlan">Porque se cuidar é sempre a melhor opção.</p>
                        </div>
                    </div>
                </div>
            </div>



            <div style="margin-left: 20px;" class="col-xl-4 col-lg-4">
                <h2>Serviços</h2>
                <button class="buttonServico">
                    <div class="insideButton">
                        <div style=" width: 108px;   margin-top: 25px;">
                            <img src="img/consorcio.png" alt="">
                            <h4 class="servicosButton-Yellow">Consórcios</h4>
                        </div>

                    </div>
                    <div style="text-align: start;padding: 10px;">
                        <h4 class="servicosButton-Green">Consórcios</h4>
                        <h4 class="servicosButton-Grey">Ache o consórcio ideal para você. Planos odontológicos, estéticos, automóveis e etc.</h4>
                    </div>
                </button>
                <button class="buttonServico">
                    <div class="insideButton"></div>
                        <div style=" width: 108px;   margin-top: 25px;">
                            <img src="img/credito.png" alt="">
                            <h4 class="servicosButton-Yellow">Crédito</h4>
                        </div>

                    </div>
                    <div style="text-align: start;padding: 10px;">
                        <h4 class="servicosButton-Green">Crédito</h4>
                        <h4 class="servicosButton-Grey">Amplie, reforme e construa. Seu crédito para crescer.</h4>
                    </div>
                </button>
                <button class="buttonServico">
                    <div class="insideButton">
                        <div style="  width: 108px;  margin-top: 25px;">
                            <img src="img/financiamentos.png" alt="">
                            <h4 class="servicosButton-Yellow">Financiamentos</h4>
                        </div>

                    </div>
                    <div style="text-align: start;padding: 10px;">
                        <h4 class="servicosButton-Green">Financiamentos</h4>
                        <h4 class="servicosButton-Grey">O financiamento que você precisa para crescer.</h4>
                    </div>
                </button>
                <button class="buttonServico">
                    <div class="insideButton">
                        <div style=" width: 108px;   margin-top: 25px;">
                            <img src="img/seemore.png" alt="">
                            <h4 class="servicosButton-Yellow">Ver Mais</h4>
                        </div>

                    </div>
                    <div style="text-align: start;padding: 10px;">
                        <h4 class="servicosButton-Green">Ver Mais</h4>
                        <h4 class="servicosButton-Grey">Mais opções de serviços que temos a oferecer.</h4>
                    </div>
                </button>
            </div>


        </div>
    </div>




</div>

<script>
    $(document).ready(function(){
        currentSlide(1);
    });
</script>