<div class="container-fluid">
          <div class="card" style="margin: 10px;">

            <div class="row" style="margin: 10px 20px 20px 20px;">
              <div class="col-xl-5 col-lg-5" >
                <div id="accordion1" style="height:70vh;margin: 20px 0px;">
                    <button class="buttonServico">
                        <div class="insideButton">
                          <div style=" width: 108px;   margin-top: 10px;">
                            <img src="img/document.png" alt="">
                          </div>
                           
                        </div> 
                        <div style="text-align: start;padding: 10px;">
                          <h4 class="servicosButton-Green">Etapa 01</h4>
                          <h4 class="servicosButton-Grey">Documentação Proprietária.</h4>
                          <h4 class="servicosButton-Grey">Status: <span style="color:#feb548">Em Andamento</span></h4>
                        </div>
                    </button>
                    <button class="buttonServico">
                        <div class="insideButton">
                          <div style=" width: 108px;   margin-top: 10px;">
                            <img src="img/document.png" alt="">
                          </div>
                           
                        </div> 
                        <div style="text-align: start;padding: 10px;">
                          <h4 class="servicosButton-Green">Etapa 01.1</h4>
                          <h4 class="servicosButton-Grey">Documentação Proprietária.</h4>
                          <h4 class="servicosButton-Grey">Status: <span style="color:#feb548">Em Andamento</span></h4>
                        </div>
                    </button>
                </div>
              </div>



              <div  class="col-xl-7 col-lg-7">
                <div class="card2" style="    place-content: center;">
                    <!-- PRIMEIRA TELA -->
                    <div style="text-align: center; margin: 60px; display:none">
                        <h2>Etapas</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Acompanhe passo à passo através de etapas, a realização do seu projeto de financiamento ou crédito rural.</h4>
                    </div>
                    <!-- PRIMEIRA TELA -->
                     <!-- ETAPA 01 -->
                    <div style="text-align: center;    padding: 0px 20px; display:none">
                        <h2>Etapa 01</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Envio de documentação pessoal</h4>
                        <img src="img/leaf.png" alt="">
                        <div class="row" style="    align-items: center;justify-content: center;">
                            <img src="img/pendente.png" alt="" >
                            <h4 style="font-size: 14px;margin-bottom: 0px; margin-left: 5px;" class="servicosButton-Grey">Pendente</h4>
                            <img src="img/analise.png" alt="" style="margin-left: 15px;">
                            <h4 style="font-size: 14px;margin-bottom: 0px;margin-left: 5px;" class="servicosButton-Grey">Em Análise</h4>
                            <img src="img/aprovado.png" alt="" style="margin-left: 15px;">
                            <h4 style="font-size: 14px;margin-bottom: 0px;margin-left: 5px;" class="servicosButton-Grey">Aprovado</h4>
                        </div>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Documentação Pessoal</h4>
                        
                        <div class="row" style="justify-content: center; margin-top:5px">
                            <div class="nameInsideEtapa">
                                <h2 style="margin:0px" class="servicosButton-Green">RG e CPF do titular;</h2>
                                <button  style="margin:0px 0px 0px 50px; border: none;background: #E5E5E5;outline: none;" class="button-Green">
                                </button>
                            </div>
                            <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png" alt="">
                            <div class="teste-Image"></div>
                        </div>

                        <button class="buttonVoltar">
                          Voltar
                        </button>
                    </div>
                    <!-- ETAPA 01 -->
                    <!-- ETAPA 02 -->
                    <div style="text-align: center;    padding: 0px 20px;display:none">
                        <h2>Etapa 02</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Declaração de Pagamento</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Declaração de pagamento para assinatura</h4>
                        <h4 style="font-size: 13px;" class="servicosButton-Grey">Realize o download de sua declaração para pagamento. E a envie após realização da assinatura.</h4>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/cloud.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Declaração: </h4>
                                    <a style="color:#21613A ;" href="">declaraçãocliente.pdf</a>
                                    
                                </div>
                            </div>
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/clip.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Documentação: <button style="border: none;outline: none; font-size: 14px;padding:2px 10px;" class="button-Green"></button></h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Grey">Envie a declaração assinada e reconhecida </h4>
                                    
                                </div>
                            </div>
                        </div>
                        <button class="buttonVoltar">
                          Voltar
                        </button>
                    </div>
                    <!-- ETAPA 02 -->
                    <!-- ETAPA 03 -->
                    <div style="text-align: center;    padding: 0px 20px; display:none">
                        <h2>Etapa 03</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Visita à propriedade</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Acompanhamento da visita à sua propriedade</h4>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/home1.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Visita agendada para:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Green">03/02/2021</h4>
                                    
                                </div>
                            </div>
                        </div>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/alarme.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Horário da visita:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Green">15h30</h4>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/location.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Endereço:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Green">Av.Endereço, Jardim Brasil, 55</h4>
                                    
                                </div>
                            </div>
                        </div>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img width="35px" src="img/clip.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Documento:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Green">Não disponível</h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ETAPA 03 -->
                    <!-- ETAPA 04 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px;">
                        <h2>Etapa 04</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Elaboração de Cadastro</h4>
                        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
                            <img src="img/sync.png" alt="">
                        </div>
                        <div style="margin-top: 15px;">
                            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status do Cadastro:</h4>
                            <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Yellow">Em progresso</h4>
                        </div>
                        
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Elaboração de Cadastro</h4>
                        <h4 style="font-size: 13px;" class="servicosButton-Grey">Estamos realizando o cadastro de seus documentos.</h4>
                    </div>
                    <!-- ETAPA 04 -->
                    <!-- ETAPA 05 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px;display:none">
                        <h2>Etapa 05</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Orçamento definitivo</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Orçamento Definitivo do Projeto</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Acompanhe passo à passo do orçamento definitivo para seu projeto</h4>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/cash.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Orçamento:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Orange">Pendente</h4>
                                    
                                </div>
                            </div>
                        </div>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/clip.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Documentação:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Orange">Pendente <button style="border: none;outline: none; font-size: 14px;padding:2px 10px;" class="button-Green"></button></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ETAPA 05 -->
                    <!-- ETAPA 06 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px;display: none;">
                        <h2>Etapa 06</h2>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Desenvolvimento do Projeto</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Desenvolvimento do Projeto</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Estamos trabalhando para oferecer a melhor estratégia para você.</h4>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/sync.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status do projeto:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Yellow">Em Progresso</h4>
                                    
                                </div>
                            </div>
                        </div>
                        <div style="width: 80%;margin-left: 20%;">
                            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                                    <img src="img/calendar.png" alt="">
                                </div>
                                <div style="margin-left: 15px;">
                                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Data prevista:</h4>
                                    <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Yellow">25/01/2021</h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ETAPA 06 -->
                    <!-- ETAPA 07 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px; display: none;">
                        <h2>Etapa 07</h2>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Envio de documentação para expansionista</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Envio de Documentação</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Estamos enviando a documentação para a Expansionista.</h4>
                        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
                            <img src="img/sync.png" alt="">
                        </div>
                        <div style="margin-top: 15px;">
                            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status da Documentação:</h4>
                            <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Yellow">Em progresso</h4>
                        </div>
                        
                    </div>
                    <!-- ETAPA 07 -->
                    <!-- ETAPA 08 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px; display: none">
                        <h2>Etapa 08</h2>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Envio de documentação ao banco</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Envio de Documentação</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Enviamos a documentação ao banca para análise.</h4>
                        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
                            <img src="img/check-yellow.png" alt="">
                        </div>
                        <div style="margin-top: 15px;">
                            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status da Documentação:</h4>
                            <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Green">Aprovada</h4>
                        </div>
                        
                    </div>
                    <!-- ETAPA 08 -->
                    <!-- ETAPA 08 Reprovado -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px; display:none">
                        <h2>Etapa 08</h2>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Envio de documentação ao banco</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Envio de Documentação</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Enviamos a documentação ao banca para análise.</h4>
                        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
                            <img src="img/reprovado.png" alt="">
                        </div>
                        <div style="margin-top: 15px;">
                            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status da Documentação:</h4>
                            <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Red">Reprovada</h4>
                        </div>
                        <button class="buttonWhatsApp1">
                          <div class="buttonWhatsApp2">
                            <img src="img/whatsapp.png" style="width:50px; padding: 10px" alt="">
                          </div>  
                          Contato
                        </button>
                    </div>
                    <!-- ETAPA 08 Reprovado -->
                    <!-- ETAPA 09 -->
                    <div style="text-align: -webkit-center;;padding: 0px 20px;display:none">
                        <h2>Etapa 09</h2>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Emissão da cédula para assinatura</h4>
                        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Emissão da Cédula</h4>
                        <h4 style="font-size: 14px;" class="servicosButton-Grey">Acompanhe a liberação da cédula de assinatura do projeto.</h4>
                        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
                            <img src="img/sync.png" alt="">
                        </div>
                        <div style="margin-top: 15px;">
                            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status da cédula:</h4>
                            <h4 style="font-size: 14px;margin-bottom: 0px;" class="servicosButton-Yellow">Em Progresso</h4>
                        </div>
                    </div>
                    <!-- ETAPA 09 -->

                </div>
              </div>


            </div>
          </div>




        </div>