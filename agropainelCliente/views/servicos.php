<div class="container-fluid">
  <div class="card" style="margin: 10px;">

    <div class="row" style="margin: 10px 20px 20px 20px;">
      <div class="col-xl-5 col-lg-5">
        <div id="accordion1" style="height:70vh;margin: 20px 0px;">
          <button class="buttonServico">
            <div class="insideButton">
              <div style=" width: 108px;   margin-top: 25px;">
                <img src="img/consorcio.png">
                <h4 class="servicosButton-Yellow">Consórcios</h4>
              </div>

            </div>
            <div style="text-align: start;padding: 10px;">
              <h4 class="servicosButton-Green">Consórcios</h4>
              <h4 class="servicosButton-Grey">Ache o consórcio ideal para você. Planos odontológicos, estéticos, automóveis e etc.</h4>
            </div>
          </button>
          <button class="buttonServico">
            <div class="insideButton">
              <div style=" width: 108px;   margin-top: 25px;">
                <img src="img/credito.png">
                <h4 class="servicosButton-Yellow">Crédito</h4>
              </div>

            </div>
            <div style="text-align: start;padding: 10px;">
              <h4 class="servicosButton-Green">Crédito</h4>
              <h4 class="servicosButton-Grey">Amplie, reforme e construa. Seu crédito para crescer.</h4>
            </div>
          </button>
          <button class="buttonServico">
            <div class="insideButton">
              <div style="  width: 108px;  margin-top: 25px;">
                <img src="img/financiamentos.png">
                <h4 class="servicosButton-Yellow">Financiamentos</h4>
              </div>

            </div>
            <div style="text-align: start;padding: 10px;">
              <h4 class="servicosButton-Green">Financiamentos</h4>
              <h4 class="servicosButton-Grey">O financiamento que você precisa para crescer.</h4>
            </div>
          </button>
          <button class="buttonServico">
            <div class="insideButton">
              <div style=" width: 108px;   margin-top: 25px;">
                <img src="img/seemore.png">
                <h4 class="servicosButton-Yellow">Ver Mais</h4>
              </div>

            </div>
            <div style="text-align: start;padding: 10px;">
              <h4 class="servicosButton-Green">Ver Mais</h4>
              <h4 class="servicosButton-Grey">Mais opções de serviços que temos a oferecer.</h4>
            </div>
          </button>
        </div>
      </div>



      <div class="col-xl-7 col-lg-7">
        <div class="card2" style="    place-content: center;">
          <div style="text-align: center; margin: 60px; display:none">
            <img src="./img/question.png" style="width:60px">
            <h2>Consórcios</h2>
            <h4 style="font-size: 18px;" class="servicosButton-Grey">Qual é o consórcio ideal para você? Navegue pelas opções e escolha o plano necessário para você. Sem complicações e rápido!</h4>
          </div>
          <div style="text-align: center;    padding: 0px 20px;">
            <h2>Consórcio Odontológico</h2>
            <div class="slideshow-container">

              <div class="mySlides ">
                <img src="img/Rectangle 5.png" style="width:100%">
              </div>
              <div class="mySlides ">
                <img src="img/Rectangle 5.png" style="width:100%">
              </div>
              <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                              <a class="next" onclick="plusSlides(1)">&#10095;</a> -->
              <div style="text-align: center;">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
              </div>
            </div>
            <h4 style="font-size: 15px;" class="servicosButton-Grey"> Está em busca do sorriso perfeito? A solução é o consórcio de serviços dentários. Garantindo um tratamento completo e de qualidade.</h4>
            <hr style="border-top: 3px solid #21613A;">
            <div class="onoffswitch">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" tabindex="0" checked>
              <label class="onoffswitch-label" for="myonoffswitch">
                <span class="onoffswitch-inner"></span>
              </label>
            </div>
            <div class="row" style="align-items: center;place-content: center;">
              <img src="img/money.png" style="margin-right: 3px;">
              <h4 style="font-size: 15px;margin-bottom: 0px;margin-right: 3px;" class="servicosButton-Grey">Valor Total:</h4>
              <div style="    background: #D2D1D1;border-radius: 20px;align-self: center;">
                <h4 style="font-size: 15px;padding: 5px;margin-bottom: 0px;" class="servicosButton-Grey">R$2.500,00</h4>
              </div>
            </div>
            <button class="buttonWhatsApp1">
              <div class="buttonWhatsApp2">
                <img src="img/whatsapp.png" style="width:50px; padding: 10px">
              </div>
              Enviar
            </button>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>