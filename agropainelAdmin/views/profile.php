<?php
include_once("../../conn/conexao.php");
session_start();

$id_user = $_SESSION['ZWxldHJpY2Ftadm'];

$sql = "SELECT * FROM user_admin WHERE id=$id_user";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $nome = $row['nome'];
  $cpf = $row['cpf'] ? $row['cpf'] : "Não possui CPF cadastrado";
  $email = $row['email'] ? $row['email'] : "Não possui email cadastrado";
  $celular = $row['celular'] ? $row['celular'] : "Não possui celular cadastrado";
  $avatar = $row['avatar'];
  // $endereco = $row['endereco'].", ".$row['numero']." - ". $row['bairro']. ", ". $row['cidade'];
}
?>

<div class="container-fluid">
  <div class="card" style="margin: 10px;">

    <div class="row" style="margin: 65px 20px 65px 20px;">

      <div class="col-xl-4 col-lg-4">
        <div class="img-profile">
          <img class="rounded-circle" src="data:image/png;base64,<?= $avatar ?>" style="width:200px; height:200px;" alt="">
          <form action="php/altera_img_admin.php" method="POST" id="form-edita-avatar" enctype="multipart/form-data">
            <input type="hidden" name="id_admin" value="<?= $id_user ?>">
            <input id="fileinput" type="file" name="img_user" style="display:none;" />
          </form>
          <div id="buttonAlterarImg" class="btn btn-user btn-block">
            <label id="selected_filename" style="margin:0;cursor:pointer;">Alterar Imagem</label>
          </div>
        </div>
      </div>
      <div style="margin-left: 20px;    " class="col-xl-7 col-lg-7">
        <div class="personal-information">
          <h2><?= $nome ?></h2>
          <h5>CPF: <?= $cpf ?></h5>
          <div style="margin-top: 25px;text-align: start;">
            <div class="email-info">
              <i class="fas fa-envelope" > </i>
              <span style="font-weight: bold;">E-mail:</span>
              <?= $email ?>
            </div>
            <div class="celular-info">
              <i class="fas fa-mobile-alt" > </i>
              <span style="font-weight: bold;">Celular:</span>
              <?= $celular ?>
            </div>
            <!-- <div class="telefone-info">
              <i class="fas fa-home" > </i>
              <span style="font-weight: bold;">Endereço:</span>
              endereco
            </div> --> 
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
      $('#buttonAlterarImg').click(function() {
        $("#fileinput").click();
      });
    });
    $('#fileinput').change(function() {
      $('#selected_filename').html($('#fileinput')[0].files[0].name);
      $('#form-edita-avatar').submit();
    });
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("dot");
      if (n > slides.length) {
        slideIndex = 1
      }
      if (n < 1) {
        slideIndex = slides.length
      }
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex - 1].style.display = "block";
      dots[slideIndex - 1].className += " active";
    }
  </script>