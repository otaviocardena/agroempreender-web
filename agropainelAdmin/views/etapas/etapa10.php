<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = 4";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_etapa = $row['status'];
}

$sql = "SELECT * FROM cliente_servico_etapa WHERE id = $id_cliente_servico_etapa AND doc_admin IS NOT NULL";
$res = mysqli_query($conn, $sql);
$doc_admin = mysqli_num_rows($res);

?>
<!-- ETAPA 04 -->
<div id="etapa10" style="text-align: center;    padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <div style="text-align: -webkit-center;">
        <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
            <img src="img/clip.png" alt="">
        </div>
    </div>
    <h2>Enviar Documentação</h2>
    <?php if ($doc_admin == 0) { ?>
        <div style="margin-top: 15px;">
            <form id="form-doc-admin" action="php/envia_arquivo_admin.php" method="POST" enctype="multipart/form-data">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Documentação:
                    <input type="hidden" id="id_cliente_servico_etapa" name="id_cliente_servico_etapa" value="<?= $id_cliente_servico_etapa ?>">
                    <input onchange="envia_arquivo()" id="input_file_admin" name="input_file_admin" type="file" style="display:none" accept=".pdf">
                    <button type="button" onclick="enviar_documento_admin()" style="border: none;outline: none; font-size: 14px;padding:2px 10px;" class="button-Green"></button>
                </h4>
            </form>
            <h4 id="indisponivel" style="font-size: 14px;margin-bottom: 0px;margin-top: 2%;" class="servicosButton-Yellow">Indisponível</h4>
        </div>
    <?php } else if ($doc_admin > 0) { ?>
        <div style="margin-top: 15px;">
            <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Documentação:
                <button onclick="deleta_documento_admin(<?= $id_cliente_servico_etapa ?>)" style="border: none;outline: none; font-size: 14px;padding:2px 10px;width:auto;border-radius:5px;" class="CancelaUsuario">Excluir</button>
            </h4>
            <a id="arquivo_admin_recente" href="php/download_arquivos_admin.php?id=<?= $id_cliente_servico_etapa ?>" style="color:#21613A;">laudo_avaliacao_rural.pdf</a>
        </div>
    <?php } ?>
    
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>
<!-- ETAPA 04 -->

<script>
    function enviar_documento_admin() {
        $('#input_file_admin').click();
    }

    function envia_arquivo() {
        $('#form-doc-admin').submit();
    }

    function deleta_documento_admin(id) {
        $.get('php/deleta_arquivo_admin.php?id=' + id);
        location.reload();
    }
</script>