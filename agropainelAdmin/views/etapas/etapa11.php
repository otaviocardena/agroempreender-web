<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_etapa = $row['status'];
}

$sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa";
$res = mysqli_query($conn, $sql);

if (mysqli_num_rows($res) == 0) {
    $icon_doc = 0;
    $stt = "Não disponível";
    $name_file = "Não disponível";
    $color = "Yellow";
    $icon_file = "img/pendente.png";
} else {
    $icon_doc = 1;
    while ($row = mysqli_fetch_array($res)) {
        $id_doc = $row['id'];
        $status_doc = $row['status'];
    }

    if ($status_doc == 1) {
        $stt = "Em análise";
        $name_file = "cedula.pdf";
        $color = "Orange";
        $icon_file = "img/analise.png";
    } else if ($status_doc == 2) {
        $stt = "Aprovado";
        $name_file = "cedula.pdf";
        $color = "Green";
        $icon_file = "img/aprovado.png";
    }
}

?>
<div id="etapa11" style="text-align: -webkit-center;padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <h4 style="font-size: 18px;" class="servicosButton-Grey">Reconhecimento de Cédula</h4>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img src="img/cloud.png" alt="">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Situação:</h4>
                <h4 id="stt_processo" style="font-size: 14px;margin-bottom: 0px; " class="servicosButton-<?= $color ?>"><?= $stt ?></h4>
            </div>
        </div>
    </div>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img src="img/clip.png" alt="">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                    Documentação:
                    <?php if ($icon_doc == 0) { ?>
                        <a style="outline:none;background: #5A5B5B;cursor:default;padding:6px;" class="icon-plusClientes" onclick="">
                            <i class="fas fa-cloud-download-alt"></i>
                        </a>
                        <div style="position:relative;">
                            <h4 style="font-size: 14px;margin-bottom: 0px; position:relative;" class="servicosButton-<?= $color ?>">
                                <label id="name_file"><?= $name_file ?></label>
                                <div style="position: absolute;right: 25px;;top: -7px;">
                                    <div class="btn-group dropright">
                                        <button id="button_altera_status" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img id="status_doc" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="<?= $icon_file ?>">
                                        </button>
                                    </div>
                                </div>
                            </h4>
                        </div>
                    <?php } else { ?>
                        <a target="_BLANK" href="php/download_arquivos.php?id=<?= $id_doc ?>&tabela=cliente_servico_etapa" style="outline:none;background: #21613A;cursor:pointer;padding:6px;" class="icon-plusClientes">
                            <i class="fas fa-cloud-download-alt" style="color:#5A5B5B"></i>
                        </a>
                        <div style="position:relative;">
                            <h4 style="font-size: 14px;margin-bottom: 0px; position:relative;" class="servicosButton-Grey">
                                <label id="name_file"><?= $name_file ?></label>
                                <div style="position: absolute;right: 25px;top: -7px;">
                                    <div class="btn-group dropright">
                                        <button disabled="" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
                                        </button>
                                        <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                            <div class="triangle-left"></div>
                                            <img style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                            <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                            <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                        </div>
                                    </div>
                                </div>
                            </h4>
                        </div>
                    <?php } ?>
                </h4>

            </div>

        </div>
    </div>

    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>

<script>
    function altera_status_documento(status, id_documento, tabela) {
        if (status == 0) {
            icon = "img/pendente.png";
            msg = "para pendente";
            stt = "<b>Não disponível</b>";
            color_stt = "#FEB548";
        } else if (status == 1) {
            icon = "img/analise.png";
            msg = "para análise";
            stt = "Em análise";
            color_stt = "#FB6F0A";
        } else if (status == 2) {
            icon = "img/aprovado.png";
            msg = "para aprovado";
            stt = "Aprovado";
            color_stt = "#21613A";
        }
        resp = confirm("Deseja realmente alterar o status do documento " + msg + "?");
        if (resp) {
            $.get('php/altera_status_documento.php?status=' + status + '&id_documento=' + id_documento + '&tabela=' + tabela, function(data) {
                if (data == 1) {
                    $('#stt_processo').html(stt);
                    if (status == 0) {
                        $('#name_file').html(stt);
                    }
                    $("#stt_processo").css("color", color_stt);
                    $("#name_file").css("color", color_stt);
                    $('#status_doc').attr('src', icon);
                    if (status == 0) {
                        $('#button_altera_status').attr('disabled', 'disabled');
                    }
                } else {
                    alert("Não foi possível realizar a alteração.");
                }
            });
        }
    }
</script>