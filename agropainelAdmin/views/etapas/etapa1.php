<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];
$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $status_etapa = $row['status'];
}

function getInfoCliente($id)
{
    global $conn;

    $info = array();

    $sql = "SELECT * FROM user_cliente WHERE id=$id";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        array_push($info, $row['nome']);
        array_push($info, $row['avatar']);
    }

    return $info;
}

function getDocsAprovados($id_cli, $id_cliente_servico)
{
    global $conn;
    global $num_etapa;
    $cont = 0;

    $sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $id_cliente_servico_etapa = $row['id'];
    }

    $sql = "SELECT * FROM documentos_cliente WHERE id_cliente = $id_cli  AND status = 2";
    $res = mysqli_query($conn, $sql);
    $cont += mysqli_num_rows($res);

    $sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente = $id_cli AND id_cliente_servico_etapa = $id_cliente_servico_etapa AND status = 2";
    $res = mysqli_query($conn, $sql);
    $cont += mysqli_num_rows($res);

    return $cont;
}

function getDocumento($id_cliente_servico, $id_cli, $tipo_doc, $tipo)
{
    global $num_etapa;
    global $conn;
    if ($tipo == 1) {
        $sql = "SELECT * FROM documentos_cliente WHERE id_cliente = $id_cli AND tipo_doc = $tipo_doc";
        $res = mysqli_query($conn, $sql);
        if (mysqli_num_rows($res) > 0) {
            $saida = array();
            while ($row = mysqli_fetch_array($res)) {
                array_push($saida, $row['id']);
                array_push($saida, $row['status']);
                array_push($saida, 'cliente');
            }
            return $saida;
        } else {
            return [];
        }
    } else {
        $sql = "SELECT id FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
        $res = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_array($res)) {
            $id_cliente_servico_etapa = $row[0];
        }

        $sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa AND id_cliente = $id_cli AND tipo_doc = $tipo_doc";
        $res = mysqli_query($conn, $sql);
        if (mysqli_num_rows($res) > 0) {
            $saida = array();
            while ($row = mysqli_fetch_array($res)) {
                array_push($saida, $row['id']);
                array_push($saida, $row['status']);
                array_push($saida, 'cliente_servico_etapa');
            }
            return $saida;
        } else {
            return [];
        }
    }
}

function getDocumentoPropriedade($id_cliente_servico, $tipo_doc)
{
    global $num_etapa;
    global $conn;

    $sql = "SELECT id FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $id_cliente_servico_etapa = $row[0];
    }

    $sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa AND tipo_doc = $tipo_doc";
    $res = mysqli_query($conn, $sql);
    if (mysqli_num_rows($res) > 0) {
        $saida = array();
        while ($row = mysqli_fetch_array($res)) {
            array_push($saida, $row['id']);
            array_push($saida, $row['status']);
            array_push($saida, 'cliente_servico_etapa');
        }
        return $saida;
    } else {
        return [];
    }
}

$sql = "SELECT * FROM cliente_servico WHERE id = $id_cliente_servico";
$res = mysqli_query($conn, $sql);
?>

<!-- ETAPA 01 -->
<div id="etapa1" style="text-align: center;    padding: 0px 20px; ">
    <h2>Etapa <?= $num_etapa ?></h2>
    <h4 style="font-size: 18px;" class="servicosButton-Grey">Envio de documentação pessoal</h4>
    <img src="img/leaf.png" alt="">
    <div class="row" style="    align-items: center;justify-content: center;">
        <img src="img/pendente.png" alt="">
        <h4 style="font-size: 14px;margin-bottom: 0px; margin-left: 5px;" class="servicosButton-Grey">Pendente</h4>
        <img src="img/analise.png" alt="" style="margin-left: 15px;">
        <h4 style="font-size: 14px;margin-bottom: 0px;margin-left: 5px;" class="servicosButton-Grey">Em Análise</h4>
        <img src="img/aprovado.png" alt="" style="margin-left: 15px;">
        <h4 style="font-size: 14px;margin-bottom: 0px;margin-left: 5px;" class="servicosButton-Grey">Aprovado</h4>
    </div>
    <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Documentação Pessoal</h4>

    <div class="row" style="justify-content: center; margin-top:5px">
        <div id="accordion" style="height: 45vh;overflow-x:hidden !important;">
            <!-- LISTA OS INTEGRANTES -->
            <?php while ($row = mysqli_fetch_array($res)) { ?>
                <!-- SE FOR PRINCIPAL -->
                <?php if (isset($row['id_cliente'])) {
                    $info_cli = array();
                    $info_cli = getInfoCliente($row['id_cliente']); // 0 -> nome, 1 -> avatar

                    $id_cli = $row['id_cliente'];
                ?>
                    <div class="card" style="background: none;border: none;width: 90%;margin-left: 5%;margin-right: 5%;margin-bottom: 5px;">
                        <div class="card-header" style="background: #E3E3E3;border-radius: 25px;" id="headingOne">
                            <h5 class="mb-0">
                                <button class="buttonNameCollapse" style="outline:none;position:relative" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <img src="data:image/png;base64,<?= $info_cli[1] ?>" class="rounded-circle" style="width:50px; height:50px" alt="">
                                    <?= $info_cli[0] ?>
                                    <div class="tasks" id="tasks_<?= $id_cli ?>"><?= getDocsAprovados($id_cli, $id_cliente_servico) ?> aprovados de 10</div>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="card-body" style="padding: 0.25rem 1.25rem;">
                                    <!-- SELECT DE TODOS OS DOCUMENTOS PARA UTILIZAR FUNÇÃO DE VERIFICAÇÃO -->
                                    <?php
                                    $sql = "SELECT 
                                                td.id AS id_doc,
                                                td.tipo,
                                                td.nome AS nome_doc
                                            FROM documentos_etapa AS de
                                            INNER JOIN tipo_documento AS td
                                                ON td.id = de.id_documento
                                            WHERE
                                                de.id_etapa = 1
                                                AND td.setor = 'Pessoal'
                                            ORDER BY
                                                de.id
                                            ";
                                    $res_cli = mysqli_query($conn, $sql);
                                    while ($row_cli = mysqli_fetch_array($res_cli)) {
                                    ?>
                                        <div class="row" style="justify-content: center; margin-top:5px">
                                            <div class="nameInsideEtapa" style="width: 80%;padding: 22px 10px;position: relative;">
                                                <h2 style="margin: 0px; width: 80%;text-align: left;" class="servicosButton-Green"><?= $row_cli['nome_doc'] ?></h2>
                                                <?php
                                                $res_doc = getDocumento($id_cliente_servico, $id_cli, $row_cli['id_doc'], $row_cli['tipo']);
                                                // echo "<br>" . $id_cliente_servico . " " . $id_cli . " " . $row_cli['id_doc'] . " " . $row_cli['tipo'];
                                                if (count($res_doc) > 1) {
                                                    // 0 -> id, 1 -> status, 2 -> cliente_servico_etapa
                                                    if (count($res_doc) > 3) {
                                                ?>
                                                        <a data-toggle="modal" data-target="#modal_varios_doc" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                                            <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                                                        </a>
                                                        <div class="modal fade" id="modal_varios_doc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-sm modal-dialog-centered" style="max-width: 730px !important;" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Documentos do Cliente</h5>
                                                                    </div>
                                                                    <div class="modal-body" style="background: #E2E2E2;">
                                                                        <div id="accordion" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                                                                            <?php
                                                                            for ($i = 0; $i < count($res_doc); $i += 3) { ?>
                                                                                <div class="form-row" style="padding: 2%;background-color: white;border-radius:15px;align-items: center;">
                                                                                    <div class="col-3">
                                                                                        <a target="_BLANK" href="php/download_arquivos.php?id=<?= $res_doc[$i] ?>&tabela=<?= $res_doc[$i + 2] ?>" style="outline:none;background: #21613A;cursor:pointer;padding:12px;" class="icon-plusClientes" onclick="">
                                                                                            <i class="fas fa-cloud-download-alt" style="color:#F6D838"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="col-6">
                                                                                        <label style="margin:0;color:#21613A;align-self: center;"><b><?= "Propriedade Pessoal" . ".pdf" ?></b></label>
                                                                                    </div>
                                                                                    <div class="col-3">
                                                                                        <div class="btn-group dropright">
                                                                                            <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                <?php if ($res_doc[$i + 1] == 0) { ?>
                                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                                                                                                <?php } else if ($res_doc[$i + 1] == 1) { ?>
                                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                                                                                                <?php } else if ($res_doc[$i + 1] == 2) { ?>
                                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                                                <?php } ?>
                                                                                            </button>
                                                                                            <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                                                                                <div class="triangle-left"></div>
                                                                                                <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                                                                                <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                                                                                <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                            <?php } ?>
                                                                            <div class="form-row" style="justify-content: center;margin-top:1%;">
                                                                                <label style="margin:0;color:#999999">Não há mais documentos no momento</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer" style="justify-content: center;background: #E2E2E2;">
                                                                        <h5 style="cursor: pointer;" data-dismiss="modal" aria-label="Close" class="servicosButton-Green">Voltar</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                            </div>
                                        <?php } else { ?>
                                            <a href="php/download_arquivos.php?id=<?= $res_doc[0] ?>&tabela=<?= $res_doc[2] ?>" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                                <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                                            </a>
                                        </div>
                                        <div class="btn-group dropright">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?php if ($res_doc[1] == 0) { ?>
                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                                                <?php } else if ($res_doc[1] == 1) { ?>
                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                                                <?php } else if ($res_doc[1] == 2) { ?>
                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                <?php } ?>
                                            </button>
                                            <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                                <div class="triangle-left"></div>
                                                <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                                <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                                <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                            </div>
                                        </div>
                                    <?php } ?>


                                </div>
                            <?php } else { ?>
                                <a style="outline:none;position: absolute;right: 15px;background: #5A5B5B;cursor:default;" class="icon-plusClientes" onclick="">
                                    <i style="color: #AFAFAF" class="fas fa-cloud-download-alt"></i>
                                </a>
                            </div>

                            <div class="btn-group dropright">
                                <button disabled="" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
                                </button>
                                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                    <div class="triangle-left"></div>
                                    <img style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                    </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if (isset($row['herdeiro_1'])) {
                    $info_cli = array();
                    $info_cli = getInfoCliente($row['herdeiro_1']);
                    $id_cli = $row['herdeiro_1'];
?>
    <div class="card" style="background: none;border: none;width: 90%;margin-left: 5%;margin-right: 5%;margin-bottom: 5px;">
        <div class="card-header" style="background: #E3E3E3;border-radius: 25px;" id="headingOne">
            <h5 class="mb-0">
                <button class="buttonNameCollapse" style="outline:none;position:relative" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <img src="data:image/png;base64,<?= $info_cli[1] ?>" class="rounded-circle" style="width:50px; height:50px" alt="">
                    <?= $info_cli[0] ?>
                    <div class="tasks" id="tasks_<?= $id_cli ?>"><?= getDocsAprovados($id_cli, $id_cliente_servico) ?> aprovados de 10
                    </div>
                </button>
            </h5>
        </div>

        <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="card-body" style="padding: 0.25rem 1.25rem;">
                    <?php
                    $sql = "SELECT 
                                                td.id AS id_doc,
                                                td.tipo,
                                                td.nome AS nome_doc
                                            FROM documentos_etapa AS de
                                            INNER JOIN tipo_documento AS td
                                                ON td.id = de.id_documento
                                            WHERE
                                                de.id_etapa = 1
                                                AND td.setor = 'Pessoal'
                                            ORDER BY
                                                de.id
                                            ";
                    $res_cli = mysqli_query($conn, $sql);
                    while ($row_cli = mysqli_fetch_array($res_cli)) {
                    ?>
                        <div class="row" style="justify-content: center; margin-top:5px">
                            <div class="nameInsideEtapa" style="width: 80%;padding: 22px 10px;position: relative;">
                                <h2 style="margin: 0px; width: 80%;text-align: left;
}" class="servicosButton-Green"><?= $row_cli['nome_doc'] ?></h2>
                                <?php
                                $res_doc = getDocumento($id_cliente_servico, $id_cli, $row_cli['id_doc'], $row_cli['tipo']);
                                // echo $id_cliente_servico . " " . $id_cli . " " . $row_cli['id_doc'] . " " . $row_cli['tipo'];
                                if (count($res_doc) > 1) {
                                    if (count($res_doc) > 3) {
                                ?>
                                        <a data-toggle="modal" data-target="#modal_varios_doc1" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                            <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                                        </a>
                                        <div class="modal fade" id="modal_varios_doc1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm modal-dialog-centered" style="max-width: 730px !important;" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Documentos do Cliente</h5>
                                                    </div>
                                                    <div class="modal-body" style="background: #E2E2E2;">
                                                        <div id="accordion" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                                                            <?php
                                                            for ($i = 0; $i < count($res_doc); $i += 3) { ?>
                                                                <div class="form-row" style="padding: 2%;background-color: white;border-radius:15px;">
                                                                    <div class="col-3">
                                                                        <a target="_BLANK" href="php/download_arquivos.php?id=<?= $res_doc[$i] ?>&tabela=<?= $res_doc[$i + 2] ?>" style="outline:none;background: #21613A;cursor:pointer;padding:12px;" class="icon-plusClientes" onclick="">
                                                                            <i class="fas fa-cloud-download-alt" style="color:#F6D838"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <label style="margin:0;color:#21613A"><b><?= "Propriedade Pessoal" . ".pdf" ?></b></label>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="btn-group dropright">
                                                                            <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <?php if ($res_doc[$i + 1] == 0) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                                                                                <?php } else if ($res_doc[$i + 1] == 1) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                                                                                <?php } else if ($res_doc[$i + 1] == 2) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                                <?php } ?>
                                                                            </button>
                                                                            <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                                                                <div class="triangle-left"></div>
                                                                                <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                                                                <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                                                                <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                            <?php } ?>
                                                            <div class="form-row" style="justify-content: center;margin-top:1%;">
                                                                <label style="margin:0;color:#999999">Não há mais documentos no momento</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer" style="justify-content: center;background: #E2E2E2;">
                                                        <h5 style="cursor: pointer;" data-dismiss="modal" aria-label="Close" class="servicosButton-Green">Voltar</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        <?php } else { ?>
                            <a href="php/download_arquivos.php?id=<?= $res_doc[0] ?>&tabela=<?= $res_doc[2] ?>" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                            </a>
                        </div>
                    <?php } ?>


                    <div class="btn-group dropright">
                        <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php if ($res_doc[1] == 0) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                            <?php } else if ($res_doc[1] == 1) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" id="status_cli" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                            <?php } else if ($res_doc[1] == 2) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" id="status_cli" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                            <?php } ?>
                        </button>
                        <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                            <div class="triangle-left"></div>
                            <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                            <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                            <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <a style="outline:none;position: absolute;right: 15px;background: #5A5B5B;cursor:default;" class="icon-plusClientes" onclick="">
                    <i style="color: #AFAFAF" class="fas fa-cloud-download-alt"></i>
                </a>
            </div>

            <div class="btn-group dropright">
                <button disabled type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
                </button>
                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                    <div class="triangle-left"></div>
                    <img style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
    </div>
    </div>
    </div>
    </div>
<?php } ?>
<?php if (isset($row['herdeiro_2'])) {
                    $info_cli = array();
                    $info_cli = getInfoCliente($row['herdeiro_2']);
                    $id_cli = $row['herdeiro_2'];
?>
    <div class="card" style="background: none;border: none;width: 90%;margin-left: 5%;margin-right: 5%;margin-bottom: 5px;">
        <div class="card-header" style="background: #E3E3E3;border-radius: 25px;" id="headingOne">
            <h5 class="mb-0">
                <button class="buttonNameCollapse" style="outline:none;position:relative" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                    <img src="data:image/png;base64,<?= $info_cli[1] ?>" class="rounded-circle" style="width:50px; height:50px" alt="">
                    <?= $info_cli[0] ?>
                    <div class="tasks" id="tasks_<?= $id_cli ?>"><?= getDocsAprovados($id_cli, $id_cliente_servico) ?> aprovados de 10
                    </div>
                </button>
            </h5>
        </div>

        <div id="collapseThree" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="card-body" style="padding: 0.25rem 1.25rem;">
                    <?php
                    $sql = "SELECT 
                                td.id AS id_doc,
                                td.tipo,
                                td.nome AS nome_doc
                            FROM documentos_etapa AS de
                            INNER JOIN tipo_documento AS td
                                 ON td.id = de.id_documento
                            WHERE
                                de.id_etapa = 1
                                AND td.setor = 'Pessoal'
                            ORDER BY
                                de.id
                            ";
                    $res_cli = mysqli_query($conn, $sql);
                    while ($row_cli = mysqli_fetch_array($res_cli)) {
                    ?>
                        <div class="row" style="justify-content: center; margin-top:5px">
                            <div class="nameInsideEtapa" style="width: 80%;padding: 22px 10px;position: relative;">
                                <h2 style="margin: 0px; width: 80%;text-align: left;
}" class="servicosButton-Green"><?= $row_cli['nome_doc'] ?></h2>
                                <?php
                                $res_doc = getDocumento($id_cliente_servico, $id_cli, $row_cli['id_doc'], $row_cli['tipo']);
                                // echo $id_cliente_servico . " " . $id_cli . " " . $row_cli['id_doc'] . " " . $row_cli['tipo'];
                                if (count($res_doc) > 1) {
                                    if (count($res_doc) > 3) {
                                ?>
                                        <a data-toggle="modal" data-target="#modal_varios_doc2" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                            <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                                        </a>
                                        <div class="modal fade" id="modal_varios_doc2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm modal-dialog-centered" style="max-width: 730px !important;" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Documentos do Cliente</h5>
                                                    </div>
                                                    <div class="modal-body" style="background: #E2E2E2;">
                                                        <div id="accordion" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                                                            <?php
                                                            for ($i = 0; $i < count($res_doc); $i += 3) { ?>
                                                                <div class="form-row" style="padding: 2%;background-color: white;border-radius:15px;">
                                                                    <div class="col-3">
                                                                        <a target="_BLANK" href="php/download_arquivos.php?id=<?= $res_doc[$i] ?>&tabela=<?= $res_doc[$i + 2] ?>" style="outline:none;background: #21613A;cursor:pointer;padding:12px;" class="icon-plusClientes" onclick="">
                                                                            <i class="fas fa-cloud-download-alt" style="color:#F6D838"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <label style="margin:0;color:#21613A"><b><?= "Propriedade Pessoal" . ".pdf" ?></b></label>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="btn-group dropright">
                                                                            <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <?php if ($res_doc[$i + 1] == 0) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                                                                                <?php } else if ($res_doc[$i + 1] == 1) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                                                                                <?php } else if ($res_doc[$i + 1] == 2) { ?>
                                                                                    <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[$i] ?>_t<?= $res_doc[$i + 2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                                <?php } ?>
                                                                            </button>
                                                                            <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                                                                <div class="triangle-left"></div>
                                                                                <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                                                                <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                                                                <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[$i] ?>,'<?= $res_doc[$i + 2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                            <?php } ?>
                                                            <div class="form-row" style="justify-content: center;margin-top:1%;">
                                                                <label style="margin:0;color:#999999">Não há mais documentos no momento</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer" style="justify-content: center;background: #E2E2E2;">
                                                        <h5 style="cursor: pointer;" data-dismiss="modal" aria-label="Close" class="servicosButton-Green">Voltar</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        <?php } else { ?>
                            <a href="php/download_arquivos.php?id=<?= $res_doc[0] ?>&tabela=<?= $res_doc[2] ?>" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                                <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                            </a>
                        </div>
                    <?php } ?>


                    <div class="btn-group dropright">
                        <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php if ($res_doc[1] == 0) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                            <?php } else if ($res_doc[1] == 1) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" id="status_cli" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                            <?php } else if ($res_doc[1] == 2) { ?>
                                <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" id="status_cli" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                            <?php } ?>
                        </button>
                        <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                            <div class="triangle-left"></div>
                            <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                            <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                            <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <a style="outline:none;position: absolute;right: 15px;background: #5A5B5B;cursor:default;" class="icon-plusClientes" onclick="">
                    <i style="color: #AFAFAF" class="fas fa-cloud-download-alt"></i>
                </a>
            </div>

            <div class="btn-group dropright">
                <button disabled type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
                </button>
                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                    <div class="triangle-left"></div>
                    <img style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                    <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
    </div>
    </div>
    </div>
    </div>
<?php } ?>
<?php } ?>
<div style="margin-bottom:15%;">
    <h2 style="color:#5A5A5A">Documentação Proprietária</h2>
    <?php
    $sql = "SELECT 
                td.id AS id_doc,
                td.tipo,
                td.nome AS nome_doc
            FROM documentos_etapa AS de
            INNER JOIN tipo_documento AS td
                ON td.id = de.id_documento
            WHERE
                de.id_etapa = 1
                AND td.setor = 'Propriedade'
            ORDER BY
                de.id
            ";
    $res_doc_prop = mysqli_query($conn, $sql);
    while ($row_doc_prop = mysqli_fetch_array($res_doc_prop)) {
    ?>
        <div class="row" style="justify-content: center; margin-top:5px">
            <div class="nameInsideEtapa" style="width: 80%;padding: 22px 10px;position: relative;">
                <h2 style="margin: 0px; width: 80%;text-align: left;
}" class="servicosButton-Green"><?= $row_doc_prop['nome_doc'] ?></h2>
                <?php
                $res_doc = getDocumentoPropriedade($id_cliente_servico, $row_doc_prop['id_doc'], $row_doc_prop['tipo']);
                // echo $id_cliente_servico . " " . $id_cli . " " . $row_doc_prop['id_doc'] . " " . $row_doc_prop['tipo'];
                if (count($res_doc) > 1) {
                ?>
                    <a href="php/download_arquivos.php?id=<?= $res_doc[0] ?>&tabela=<?= $res_doc[2] ?>" style="outline:none;position: absolute;right: 15px;" class="icon-plusClientes" onclick="">
                        <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                    </a>
            </div>

            <div class="btn-group dropright">
                <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php if ($res_doc[1] == 0) { ?>
                        <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/pendente.png">
                    <?php } else if ($res_doc[1] == 1) { ?>
                        <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" id="status_cli" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png">
                    <?php } else if ($res_doc[1] == 2) { ?>
                        <img id="status_cli<?= $id_cli ?>_doc<?= $res_doc[0] ?>_t<?= $res_doc[2] ?>" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                    <?php } ?>
                </button>
                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                    <div class="triangle-left"></div>
                    <img onclick="altera_status_documento(0,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                    <img onclick="altera_status_documento(1,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                    <img onclick="altera_status_documento(2,<?= $id_cli ?>,<?= $res_doc[0] ?>,'<?= $res_doc[2] ?>')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                </div>
            </div>
        </div>
    <?php } else { ?>
        <a style="outline:none;position: absolute;right: 15px;background: #5A5B5B;cursor:default;" class="icon-plusClientes" onclick="">
            <i style="color: #AFAFAF" class="fas fa-cloud-download-alt"></i>
        </a>

</div>

<div class="btn-group dropright">
    <button disabled type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img style="width: 30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
    </button>
    <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
        <div class="triangle-left"></div>
        <img style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
        <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
        <img style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
    </div>
</div>
</div>
<?php } ?>
<?php } ?>
</div>

</div>

</div>

<?php if ($status_etapa == 1) { ?>
    <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
        Finalizar Etapa
    </button>
<?php } else if ($status_etapa == 2) { ?>
    <button class="buttonVoltar" style="cursor:default">
        Etapa Finalizada
    </button>
<?php } ?>
</div>
<script>
    function altera_status_documento(status, id_cli, id_documento, tabela) {
        if (status == 0) {
            img = "img/pendente.png";
            msg = "para pendente";
        } else if (status == 1) {
            img = "img/analise.png";
            msg = "para análise";
        } else if (status == 2) {
            img = "img/aprovado.png";
            msg = "para aprovado";
        }
        resp = confirm("Deseja realmente alterar o status do documento " + msg + "?");
        if (resp) {
            $.get('php/altera_status_documento.php?status=' + status + '&id_documento=' + id_documento + '&tabela=' + tabela, function(data) {
                if (data == 1) {
                    document.getElementById('status_cli' + id_cli + '_doc' + id_documento + '_t' + tabela).setAttribute('src', img);
                } else {
                    alert("Não foi possível realizar a alteração.");
                }
            });
        }
    }
</script>
<!-- ETAPA 01 -->