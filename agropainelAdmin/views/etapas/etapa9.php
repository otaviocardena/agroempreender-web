<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_processo_interno = $row['status_processo_interno'];
    $status_etapa = $row['status'];
}

if ($status_processo_interno == 0 || $status_processo_interno == 1) {
    $stt = "Em progresso";
    $color = "Orange";
    $img_processo = "img/sync.png";
    $img_icon = "img/analise.png";
} else {
    $stt = "Liberada";
    $color = "Green";
    $img_processo = "img/edit_process.png";
    $img_icon = "img/aprovado.png";
}
?>
<div id="etapa9" style="text-align: -webkit-center;;padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <h4 style="font-size: 14px;" class="servicosButton-Grey">Emissão da cédula para assinatura</h4>
    <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Emissão da Cédula</h4>
    <h4 style="font-size: 14px;" class="servicosButton-Grey">Acompanhe a liberação da cédula de assinatura do projeto.</h4>
    <div style="background:#21613A;border-radius: 100px;width: fit-content;padding: 25px;">
        <img id="img_processo" src="<?= $img_processo ?>">
    </div>
    <div style="margin-top: 15px;">
        <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status da cédula:</h4>
        <div style="position:relative;">
            <h4 style="font-size: 24px;margin-bottom: 0px; position:relative; margin:0px 30%" class="servicosButton-<?= $color ?>">
                <label id="stt_processo"><?= $stt ?></label>
                <div style="position: absolute;right: -5px;top: -7px;">
                    <div class="btn-group dropright">
                        <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img id="icon_status_processo" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="<?= $img_icon ?>">
                        </button>
                        <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                            <div class="triangle-left"></div>
                            <img onclick="atualiza_processo(1)" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                            <img onclick="atualiza_processo(2)" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                        </div>
                    </div>
                </div>
            </h4>
        </div>
    </div>
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>

<script>
    function atualiza_processo(status) {
        if (status == 1) {
            icon = "img/analise.png";
            msg = "para em progresso";
            status_msg = "Em progresso";
            img_processo = "img/sync.png";
            color_stt = "#FB6F0A";
        } else if (status == 2) {
            icon = "img/aprovado.png";
            msg = "para liberada";
            status_msg = "Liberada";
            img_processo = "img/edit_process.png";
            color_stt = "#21613A";
        }

        resp = confirm("Deseja realmente alterar o status da cédula " + msg + "?");

        if (resp) {
            $.get('php/atualiza_processo.php?status=' + status + '&id_cliente_servico_etapa=' + <?= $id_cliente_servico_etapa ?>, function(data) {
                if (data == 1) {
                    $('#stt_processo').html(status_msg);
                    document.getElementById('icon_status_processo').setAttribute('src', icon);
                    $("#stt_processo").css("color", color_stt);
                    document.getElementById('img_processo').setAttribute('src', img_processo);

                } else {
                    alert("Não foi possível realizar a alteração.");
                }
            });
        }
    }
</script>