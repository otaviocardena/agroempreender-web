<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT id_cliente FROM cliente_servico WHERE id = $id_cliente_servico";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente = $row[0];
}

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_etapa = $row['status'];
}

$sql = "SELECT * FROM cliente_servico_etapa WHERE id = $id_cliente_servico_etapa AND doc_admin IS NOT NULL";
$res = mysqli_query($conn, $sql);
$arquivo_cont = mysqli_num_rows($res);

$sql = "SELECT * FROM visitas_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa";
$res = mysqli_query($conn, $sql);
$visita = mysqli_num_rows($res);

if ($visita > 0) {
    while ($row = mysqli_fetch_array($res)) {
        $data = date('d/m/Y', strtotime($row['data']));
        $horario = date('H:i', strtotime($row['horario']));
        $endereco = $row['endereco'];
    }
}

?>
<!-- ETAPA 03 -->
<div id="etapa3" style="text-align: center;    padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <?php if ($visita == 0) { ?>
        <form action="php/cadastra_visita.php" id="form-visita" method="POST">
            <input type="hidden" id="id_cliente_servico_etapa" name="id_cliente_servico_etapa" value="<?= $id_cliente_servico_etapa ?>">
            <h4 style="font-size: 18px;" class="servicosButton-Grey">Visita à propriedade</h4>
            <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Acompanhamento da visita à sua propriedade</h4>
            <div style="width: 80%;margin-left: 20%;">
                <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                    <div style="background:#21613A; padding:20px;border-radius: 100px;">
                        <img src="img/home1.png" alt="">
                    </div>
                    <div style="margin-left: 15px;">
                        <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Visita
                            agendada para:</h4>
                        <input id="dia_visita" name="dia_visita" style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="date">
                    </div>
                </div>
            </div>
            <div style="width: 80%;margin-left: 20%;">
                <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                    <div style="background:#21613A; padding:20px;border-radius: 100px;">
                        <img src="img/alarme.png" alt="">
                    </div>
                    <div style="margin-left: 15px;">
                        <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Horário
                            da visita:</h4>
                        <input id="horario_visita" name="horario_visita" style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="time">
                    </div>
                </div>
            </div>
            <div style="width: 80%;margin-left: 20%;">
                <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                    <div style="background:#21613A; padding:20px;border-radius: 100px;">
                        <img src="img/location.png" alt="">
                    </div>
                    <div style="margin-left: 15px;">
                        <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                            Endereço:</h4>
                        <input id="endereco_visita" name="endereco_visita" style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="text">
                    </div>
                </div>
            </div>
            <div style="width: 80%;margin-left: 20%;">
                <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                    <div style="background:#21613A; padding:20px;border-radius: 100px;text-align-last: center;width:75px;height:75px">
                        <img src="img/clip2.png" alt="">
                    </div>
                    <div style="margin-left: 15px;">
                        <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                            Documentação:
                            <br>
                            <a style="color:#FB6F0A;">Ainda não pode ser inserida</a>
                            <!-- necessário botão de X para excluir arquivo -->
                        </h4>
                    </div>
                </div>
            </div>
            <div style="width: 80%;margin-left: 20%;">
                <div class="row">
                    <button onclick="confirmar_visita()" type="button" class="buttonVoltar">
                        Confirmar Visita
                    </button>
                </div>
            </div>
        </form>
    <?php } else if ($visita > 0) { ?>
        <h4 style="font-size: 18px;" class="servicosButton-Grey">Visita à propriedade</h4>
        <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Acompanhamento da visita à sua propriedade</h4>
        <div style="width: 80%;margin-left: 20%;">
            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                    <img src="img/home1.png" alt="">
                </div>
                <div style="margin-left: 15px;">
                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Visita
                        agendada para:</h4>
                    <div style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="date">
                        <?= $data ?>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 80%;margin-left: 20%;">
            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                    <img src="img/alarme.png" alt="">
                </div>
                <div style="margin-left: 15px;">
                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Horário
                        da visita:</h4>
                    <div id="horario_visita" name="horario_visita" style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="time">
                        <?= $horario ?>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 80%;margin-left: 20%;">
            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                <div style="background:#21613A; padding:20px;border-radius: 100px;">
                    <img src="img/location.png" alt="">
                </div>
                <div style="margin-left: 15px;">
                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                        Endereço:</h4>
                    <div id="endereco_visita" name="endereco_visita" style="outline:none;border-radius: 10px;border: none;background: #cacaca;color: #656565;padding-left: 5px;" type="text">
                        <?= $endereco ?>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 80%;margin-left: 20%;">
            <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
                <div style="background:#21613A; padding:20px;border-radius: 100px;text-align-last: center;width:75px;height:75px">
                    <img src="img/clip2.png">
                </div>
                <div style="margin-left: 15px">
                    <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                        Documentação:
                        <?php if ($arquivo_cont == 0) { ?>
                            <form id="form-doc-admin" action="php/envia_arquivo_admin.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="id_cliente_servico_etapa" name="id_cliente_servico_etapa" value="<?= $id_cliente_servico_etapa ?>">
                                <input onchange="envia_arquivo()" id="input_file_admin" name="input_file_admin" type="file" style="display:none" accept=".pdf">
                            </form>
                            <button onclick="enviar_documento_admin()" style="border: none;outline: none; font-size: 14px;padding:2px 10px;" class="button-Green"></button>
                    </h4>
                    <a id="arquivo_admin_recente" href="php/download_arquivos_admin.php?id=<?= $id_cliente_servico_etapa ?>" style="color:#21613A; display: none;">declaraçãocliente.pdf</a>
                <?php } else { ?>
                    <button onclick="deleta_documento_admin(<?= $id_cliente_servico_etapa ?>)" style="border: none;outline: none; font-size: 14px;padding:2px 10px;width:auto;border-radius:5px;" class="CancelaUsuario">Excluir</button>
                    </h4>
                    <a href="php/download_arquivos_admin.php?id=<?= $id_cliente_servico_etapa ?>" style="color:#21613A ;">avaliacaorealizada.pdf</a>
                    <!-- necessário botão de X para excluir arquivo -->
                <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>
<!-- ETAPA 03 -->
<script>
    function confirmar_visita() {
        $('#form-visita').submit();
    }

    function enviar_documento_admin() {
        $('#input_file_admin').click();
    }

    function envia_arquivo() {
        $('#form-doc-admin').submit();
        $('#arquivo_admin_recente').css("display", "block");
    }

    function deleta_documento_admin(id) {
        $.get('php/deleta_arquivo_admin.php?id=' + id);
        window.location.href = "index.php#plano-cliente-info.php?id_cliente_aberto=" + <?= $id_cliente ?> + "&id_cliente_servico=" + <?= $id_cliente_servico ?>;
        location.reload();
    }
</script>