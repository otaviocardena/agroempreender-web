<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $orcamento = $row['adicional'] ? $row['adicional'] : "";
    $status_etapa = $row['status'];
}

$sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa AND (status = 1 OR status = 2)";
$res_doc = mysqli_query($conn, $sql);
$info_doc = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($info_doc)) {
    $status_doc = $row['status'];
}

if (mysqli_num_rows($res_doc) == 0) {
    $disabled = "disabled";
    $placeholder = "Esperando documentos";
    $status_doc = "Não disponível";
} else {
    $disabled = "";
    $placeholder = "Digite o valor do orçamento";

    if ($status_doc == 1) {
        $color = "Orange";
        $icon_status = "img/analise.png";
        $status_doc = "Em análise";
    } else {
        $color = "Green";
        $icon_status = "img/aprovado.png";
        $status_doc = "Aprovado(s)";
    }
}

?>
<div id="etapa5" style="text-align: -webkit-center;padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <h4 style="font-size: 18px;" class="servicosButton-Grey">Orçamento definitivo</h4>
    <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Orçamento Definitivo do Projeto</h4>
    <h4 style="font-size: 14px;" class="servicosButton-Grey">Acompanhe passo à passo do orçamento definitivo para seu projeto</h4>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img src="img/cash.png" alt="">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Orçamento:</h4>
                <input value="<?= $orcamento ?>" onkeyup="atualiza_orcamento(this,event)" type="number" step="0.01" id="orcamento" name="orcamento" placeholder="<?= $placeholder ?>" style="border:none; border-radius: 15px; background:#CBCBCB;font-weight:bold;color:#21613A;outline:none" <?= $disabled ?>>
            </div>
        </div>
    </div>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img src="img/clip.png" alt="">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                    Documentação:
                    <?php if (mysqli_num_rows($res_doc) == 0) { ?>
                        <a style="outline:none;background: #5A5B5B;cursor:default;padding:6px;" class="icon-plusClientes" onclick="">
                            <i class="fas fa-cloud-download-alt"></i>
                        </a>
                    <?php } else { ?>
                        <a style="outline:none;background: #21613A;cursor:pointer;padding:6px;" class="icon-plusClientes" onclick="abre_modal_orcamentos()">
                            <i class="fas fa-cloud-download-alt" style="color:#F6D838"></i>
                        </a>
                    <?php } ?>
                </h4>
                <div style="position:relative;">
                    <h4 style="font-size: 14px;margin-bottom: 0px; position:relative;" class="servicosButton-<?= $color ?>">
                        <label id="stt_doc"><?= $status_doc ?></label>
                        <div style="position: absolute;right: 30px;top: -7px;">
                            <div class="btn-group dropright">
                                <button <?= $disabled ?> id="button_altera_status" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img id="status_doc" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="<?= $icon_status ?>">
                                </button>
                                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                    <div class="triangle-left"></div>
                                    <img onclick="altera_status_documento(0,<?= $id_cliente_servico_etapa ?>,'cliente_servico_etapa','etapa')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                                    <img onclick="altera_status_documento(1,<?= $id_cliente_servico_etapa ?>,'cliente_servico_etapa','etapa')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                    <img onclick="altera_status_documento(2,<?= $id_cliente_servico_etapa ?>,'cliente_servico_etapa','etapa')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                </div>
                            </div>
                        </div>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>

<div class="modal fade" id="arquivos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" style="max-width: 730px !important;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Orçamentos do Cliente</h5>
            </div>
            <div class="modal-body" style="background: #E2E2E2;">
                <div id="accordion" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                    <?php
                    $i = 1;
                    while ($row = mysqli_fetch_array($res_doc)) { ?>
                        <div class="form-row" style="padding: 2%;background-color: white;border-radius:15px;">
                            <div class="col-3">
                                <a target="_BLANK" href="php/download_arquivos.php?id=<?= $row['id'] ?>&tabela=cliente_servico_etapa" style="outline:none;background: #21613A;cursor:pointer;padding:12px;" class="icon-plusClientes" onclick="">
                                    <i class="fas fa-cloud-download-alt" style="color:#F6D838"></i>
                                </a>
                            </div>
                            <div class="col-6">
                                <label style="margin:0;color:#21613A"><b><?= "documento" . $i . ".pdf" ?></b></label>
                            </div>
                            <div class="col-3">
                                <label style="margin:0;color:#21613A"><b><?= date('d/m/Y', strtotime($row['data_cad'])) ?></b></label>
                            </div>
                        </div>
                        <br>
                    <?php
                        $i++;
                    } ?>
                    <div class="form-row" style="justify-content: center;margin-top:5%;">
                        <label style="margin:0;color:#999999">Não há mais documentos no momento</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="justify-content: center;background: #E2E2E2;">
                <h5 style="cursor: pointer;" data-dismiss="modal" aria-label="Close" class="servicosButton-Green">Voltar</h5>
            </div>
        </div>
    </div>
</div>

<script>
    function abre_modal_orcamentos() {
        $("#arquivos").modal("show");
    }

    function altera_status_documento(status, id_documento, tabela, forma) {
        if (status == 0) {
            icon = "img/pendente.png";
            msg = "para pendente";
            stt = "Pendente";
            color_stt = "#FEB548";
        } else if (status == 1) {
            icon = "img/analise.png";
            msg = "para análise";
            stt = "Em análise";
            color_stt = "#FB6F0A";
        } else if (status == 2) {
            icon = "img/aprovado.png";
            msg = "para aprovado";
            stt = "Aprovado";
            color_stt = "#21613A";
        }
        resp = confirm("Deseja realmente alterar o status do documento " + msg + "?");
        if (resp) {
            $.get('php/altera_status_documento.php?status=' + status + '&id_documento=' + id_documento + '&tabela=' + tabela + "&forma=" + forma, function(data) {
                if (data == 1) {
                    $('#stt_doc').html(stt);
                    $("#stt_doc").css("color", color_stt);
                    $('#status_doc').attr('src', icon);
                    if (status == 0) {
                        $('#button_altera_status').attr('disabled', 'disabled');
                    }
                } else {
                    alert("Não foi possível realizar a alteração.");
                }
            });
        }
    }

    function atualiza_orcamento(obj, e) {
        console.log(e);
        if (e.keyCode == 13) {
            $.get('php/atualiza_adicional.php?id_cliente_servico_etapa=' + <?= $id_cliente_servico_etapa ?> + "&value=" + obj.value, function(data) {
                if (data == "OK") {
                    $('#' + obj.id).blur();
                    alert("Orçamento atualizado.");
                } else {
                    alert("Não foi possível atualizar o orçamento.");
                }
            });
        }
    }
</script>