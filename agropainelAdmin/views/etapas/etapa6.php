<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_processo_interno = $row['status_processo_interno'];
    $status_etapa = $row['status'];

    if (isset($row['adicional'])) {
        $adicional = $row['adicional'];
    } else {
        $adicional = "";
    }
}

if ($status_processo_interno == 0 || $status_processo_interno == 1) {
    $stt = "Em progresso";
    $color = "Orange";
    $img_processo = "img/sync.png";
    $img_icon = "img/analise.png";
    $attr_data = "value='" . $adicional . "'";
    $data_texto = "prevista";
} else {
    $stt = "Finalizado";
    $color = "Green";
    $img_processo = "img/check-yellow.png";
    $img_icon = "img/aprovado.png";
    $attr_data = "disabled value='" . $adicional . "'";
    $data_texto = "de conclusão";
}
?>
<div id="etapa6" style="text-align: -webkit-center;;padding: 0px 20px;">
    <h2>Etapa 06</h2>
    <h4 style="font-size: 14px;" class="servicosButton-Grey">Desenvolvimento do Projeto</h4>
    <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Desenvolvimento do Projeto</h4>
    <h4 style="font-size: 14px;" class="servicosButton-Grey">Estamos trabalhando para oferecer a melhor estratégia para você.</h4>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img id="img_processo" src="<?= $img_processo ?>">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Status do projeto:</h4>
                <div style="position:relative;">
                    <h4 style="font-size: 14px;margin-bottom: 0px; position:relative;" class="servicosButton-<?= $color ?>">
                        <label id="stt_processo"><?= $stt ?></label>
                        <div style="position: absolute;right: -27px;top: -7px;">
                            <div class="btn-group dropright">
                                <button type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;cursor:default;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img id="icon_status_processo" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="<?= $img_icon ?>">
                                </button>
                                <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                                    <div class="triangle-left"></div>
                                    <img onclick="atualiza_processo(1)" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                                    <img onclick="atualiza_processo(2)" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                                </div>
                            </div>
                        </div>
                    </h4>
                </div>

            </div>
        </div>
    </div>
    <div style="width: 80%;margin-left: 20%;">
        <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
            <div style="background:#21613A; padding:20px;border-radius: 100px;">
                <img src="img/calendar.png" alt="">
            </div>
            <div style="margin-left: 15px;">
                <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">Data <?= $data_texto ?>:</h4>
                <input onkeyup="atualiza_data(this,event)" <?= $attr_data ?> type="text" id="data_processo" name="data_processo" style="border:none; border-radius: 15px; background:#CBCBCB;font-weight:bold;color:#21613A;outline:none">

            </div>
        </div>
    </div>
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        $('#data_processo').mask('99/99/9999');
        return false;
    });

    function atualiza_input_data(status) {
        if (status == 1) {
            $('#data_processo').removeAttr('disabled');
        } else if (status == 2) {
            $('#data_processo').attr('disabled', 'disabled');
        }
    }

    function atualiza_data_final() {
        var data_prevista = $('#data_processo').val();

        var date = new Date();
        var dia = date.getDate().toString();
        if (dia < 10) dia = "0" + dia;
        var mes = date.getMonth().toString();
        if (mes < 10) mes = "0" + mes;
        var ano = date.getFullYear().toString();
        var data_atual = dia + "/" + mes + "/" + ano;

        if (data_prevista.toString() != data_atual.toString()) {
            $.get("php/atualiza_data_final.php?id_cliente_servico_etapa=" + <?= $id_cliente_servico_etapa ?> + "&data=" + data_atual);
        }
    }

    function atualiza_processo(status) {
        if (status == 1) {
            icon = "img/analise.png";
            msg = "para em progresso";
            status_msg = "Em progresso";
            img_processo = "img/sync.png";
            color_stt = "#FB6F0A";
        } else if (status == 2) {
            icon = "img/aprovado.png";
            msg = "para realizado";
            status_msg = "Realizado";
            img_processo = "img/check-yellow.png";
            color_stt = "#21613A";
        }

        resp = confirm("Deseja realmente alterar o status do projeto " + msg + "?");

        if (resp) {
            $.get('php/atualiza_processo.php?status=' + status + '&id_cliente_servico_etapa=' + <?= $id_cliente_servico_etapa ?>, function(data) {
                if (data == 1) {
                    atualiza_input_data(status);
                    $('#stt_processo').html(status_msg);
                    document.getElementById('icon_status_processo').setAttribute('src', icon);
                    $("#stt_processo").css("color", color_stt);
                    document.getElementById('img_processo').setAttribute('src', img_processo);

                    if (status == 2) {
                        atualiza_data_final();
                    }

                } else {
                    alert("Não foi possível realizar a alteração.");
                }
            });
        }
    }

    function atualiza_data(obj, e) {
        console.log(e);
        if (e.keyCode == 13) {
            $.get('php/atualiza_adicional.php?id_cliente_servico_etapa=' + <?= $id_cliente_servico_etapa ?> + "&value=" + obj.value, function(data) {
                if (data == "OK") {
                    $('#' + obj.id).blur();
                    alert("Data atualizada.");
                } else {
                    alert("Não foi possível atualizar a data.");
                }
            });
        }
    }
</script>