<?php
include_once('../../../conn/conexao.php');
$id_cliente_servico = $_GET['id_cliente_servico'];
$num_etapa = $_GET['num_etapa'];

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico_etapa = $row['id'];
    $status_etapa = $row['status'];
    // $status_processo_interno = $row['status_processo_interno'];
}

$sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = 5";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $val_orcamento = number_format((float)$row['adicional'], 2, ",", ".");
}

$sql = "SELECT * FROM orcamento_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa";
$res_orcamento = mysqli_query($conn, $sql);

$sql = "SELECT SUM(valor) as soma FROM orcamento_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa";
$res_soma = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_soma)) {
    $soma_orcamento = number_format($row[0], 2, ",", ".");
}
?>
<div id="etapa13" style="text-align: -webkit-center;padding: 0px 20px;">
    <h2>Etapa <?= $num_etapa ?></h2>
    <div class="form-row" style="place-content: center;">
        <div style="display:block; margin-right: 15px;">
            <div class="titulo-12" style="text-align-last: right;">
                Orçamento:
            </div>
            <div class="valor-12" style="color:#21613A">
                R$<?= $val_orcamento ?>
            </div>
        </div>
        <div id="divider-12">
        </div>
        <div style="display:block; margin-left: 15px;">
            <div class="titulo-12" style="text-align-last: left;color:#D34747">
                Saída:
            </div>
            <div style="color:#21613A">
                R$<?= $soma_orcamento ?>
            </div>
        </div>
    </div>
    <div id="accordion1" style="width:100%;background:#D9D9D9;height:25vh;margin: 20px 0px;overflow-x:hidden;border-radius:15px">
        <?php while ($row = mysqli_fetch_array($res_orcamento)) { ?>
            <div class="form-row r-12">
                <div class="image-div-12">
                    <a href="php/download_orcamento.php?id=<?= $row['id'] ?>" style="padding: 0px 12px;display: block;cursor:pointer;">
                        <img src="img/cloud2.png" alt="">
                        <h5 style="color:#F6D838;font-size:10px">pdf</h5>
                    </a>
                </div>
                <div class="information-12">
                    <div style="display:flex;place-items: center;">
                        <h2 class="title-row-12"><?= $row['titulo'] ?></h2>
                        <h5 class="descrip-row-12"><?= date('d/m/Y', strtotime($row['data_cad'])) ?></h5>
                    </div>
                    <div style="font-size:12px">
                        Motivo: <?= $row['motivo'] ?>
                    </div>
                </div>
                <div>
                    <h2 style="color:#D34747">
                        -<?= "R$" . number_format($row['valor'], 2, ",", ".") ?>
                    </h2>
                </div>
                <a onclick="exclue_orcamento(<?= $row['id'] ?>)" style="border:none; outline:none;right: 50px;" class="button-edit-12">
                    <div style="padding: 0px 12px;">
                        <img src="img/reprovado.png" width="14" height="14">
                    </div>
                </a>
                <button onclick="edit_orcamento(<?= $row['id'] ?>)" data-toggle="modal" style="border:none; outline:none" class="button-edit-12">
                    <div style="padding: 0px 12px;">
                        <img src="img/edit2.png" alt="">
                    </div>
                </button>
            </div>
        <?php } ?>

        <button data-toggle="modal" data-target="#modalAdicionar" style="background: none;border: none;outline: none;margin-bottom: 15px">
            <img src="img/plus.png" alt="">
            <span style="color:#21613A;font-weight:bold">Adicionar novo lançamento</span>
        </button>
    </div>
    <?php if ($status_etapa == 1) { ?>
        <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
            Finalizar Etapa
        </button>
    <?php } else if ($status_etapa == 2) { ?>
        <button class="buttonVoltar" style="cursor:default">
            Etapa Finalizada
        </button>
    <?php } ?>
</div>


<div class="modal fade bd-example-modal-lg" id="modalAdicionar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg " role="document">
        <div class="modal-content" style="border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/cadastra_orcamento.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id_cliente_servico_etapa" name="id_cliente_servico_etapa" value="<?= $id_cliente_servico_etapa ?>">
                    <h2 style="text-align-last: center;">Novo Lançamento</h2>
                    <div style="text-align: -webkit-center;">
                        <div style="white-space: pre-line;">
                            <div class="form-row">
                                <input name="titulo" id="titulo" type="text" style="outline:none; width:40%;" class="criar-lancamento" placeholder="Título" required>
                                <input name="valor" id="valor" step="0.01" type="number" style="outline:none; width:40%;" class="criar-lancamento" placeholder="Valor" required>
                                <input onchange="altera_icone('')" type="file" name="doc_orcamento" id="doc_orcamento" style="display:none" accept=".pdf" required>
                                <button id="btn_file_orcamento" type="button" onclick="abre_input_doc('')" style="display: flex;align-items: center;justify-content: center;outline:none;background: #5A5B5B;cursor:pointer;padding:6px;height: 40px;width: 40px;" class="icon-plusClientes">
                                    <i id="icon_file_orcamento" class="fas fa-cloud-download-alt" style="color:#AFAFAF"></i>
                                </button>
                            </div>
                            <div class="form-row">
                                <input name="cod_banco" id="cod_banco" type="text" style="outline:none;width:25%" class="criar-lancamento" placeholder="Cod. Banco" required>
                                <input name="agencia" id="agencia" type="text" style="outline:none;width:25%" class="criar-lancamento" placeholder="Agencia" required>
                                <input name="conta" id="conta" type="text" style="outline:none;width:35%" class="criar-lancamento" placeholder="Conta" required>
                            </div>
                            <div class="form-row">
                                <input name="titular" id="titular" type="text" style="outline:none;width:45%" class="criar-lancamento" placeholder="Titular da Conta" required>
                                <input name="cnpj" id="cnpj" type="text" style="width:44%;outline:none" class="criar-lancamento" placeholder="CNPJ" required>
                            </div>
                            <div class="form-row">
                                <textarea name="motivo" id="motivo" maxlength="500" style="width:94% !important;outline:none;resize: vertical;" type="text" class="criar-lancamento" placeholder="Motivo" required></textarea>
                            </div>
                        </div>

                        <div style="display: flex;place-content: center;justify-content: space-between;">
                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Confirmar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg " role="document">
        <div class="modal-content" style="border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/edita_orcamento.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id_orcamento" id="id_orcamento">
                    <h2 style="text-align-last: center;">Editar Lançamento</h2>
                    <div style="text-align: -webkit-center;">
                        <div style="white-space: pre-line;">
                            <div class="form-row">
                                <input name="titulo_edit" id="titulo_edit" type="text" style="outline:none; width:40%;" class="criar-lancamento" placeholder="Título" required>
                                <input name="valor_edit" id="valor_edit" step="0.01" type="number" style="outline:none; width:40%;" class="criar-lancamento" placeholder="Valor" required>
                                <input onchange="altera_icone('_edit')" type="file" name="doc_orcamento_edit" id="doc_orcamento_edit" style="display:none" accept=".pdf">
                                <button id="btn_file_orcamento_edit" type="button" onclick="abre_input_doc('_edit')" style="display: flex;align-items: center;justify-content: center;outline:none;background: #5A5B5B;cursor:pointer;padding:6px;height: 40px;width: 40px;" class="icon-plusClientes">
                                    <i id="icon_file_orcamento_edit" class="fas fa-cloud-download-alt" style="color:#AFAFAF"></i>
                                </button>
                            </div>
                            <div class="form-row">
                                <input name="cod_banco_edit" id="cod_banco_edit" type="text" style="outline:none;width:25%" class="criar-lancamento" placeholder="Cod. Banco" required>
                                <input name="agencia_edit" id="agencia_edit" type="text" style="outline:none;width:25%" class="criar-lancamento" placeholder="Agencia" required>
                                <input name="conta_edit" id="conta_edit" type="text" style="outline:none;width:35%" class="criar-lancamento" placeholder="Conta" required>
                            </div>
                            <div class="form-row">
                                <input name="titular_edit" id="titular_edit" type="text" style="outline:none;width:45%" class="criar-lancamento" placeholder="Titular da Conta" required>
                                <input name="cnpj_edit" id="cnpj_edit" type="text" style="width:44%;outline:none" class="criar-lancamento" placeholder="CNPJ" required>
                            </div>
                            <div class="form-row">
                                <textarea name="motivo_edit" id="motivo_edit" maxlength="500" style="width:94% !important;outline:none;resize: vertical;" type="text" class="criar-lancamento" placeholder="Motivo" required></textarea>
                            </div>
                        </div>

                        <div style="display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>

                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Confirmar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function abre_input_doc(txt) {
        $('#doc_orcamento' + txt).click();
    }

    function altera_icone(txt) {
        $("#btn_file_orcamento" + txt).css("background", "#21613A");
        $("#icon_file_orcamento" + txt).css("color", "#F6D838");
    }

    function edit_orcamento(id) {
        $.get("php/get_orcamento.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_orcamento").val(id);
            $("#titulo_edit").val(json[0].titulo);
            $("#valor_edit").val(json[1].valor);
            $("#cod_banco_edit").val(json[2].cod_banco);
            $("#agencia_edit").val(json[3].agencia);
            $("#conta_edit").val(json[4].conta);
            $("#titular_edit").val(json[5].titular);
            $("#cnpj_edit").val(json[6].cnpj);
            $("#motivo_edit").val(json[7].motivo);

            $('#modalEditar').modal('show');

        });
    }

    function exclue_orcamento(id) {
        var resp = confirm("Realmente deseja excluir o orçamento?");
        if (resp) {
            $.get("php/excluir_orcamento.php?id=" + id, function(data) {
                if (data == "OK") {
                    location.reload();
                } else {
                    console.log(data);
                }
            });
        }
    }
</script>