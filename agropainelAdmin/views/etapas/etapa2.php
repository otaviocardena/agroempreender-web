  <?php
    include_once('../../../conn/conexao.php');
    $id_cliente_servico = $_GET['id_cliente_servico'];
    $num_etapa = $_GET['num_etapa'];

    $sql = "SELECT id_cliente FROM cliente_servico WHERE id = $id_cliente_servico";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $id_cliente = $row[0];
    }

    $sql = "SELECT * FROM cliente_servico_etapa WHERE id_cliente_servico = $id_cliente_servico AND etapa = $num_etapa";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $id_cliente_servico_etapa = $row['id'];
        $status_etapa = $row['status'];
    }

    $sql = "SELECT * FROM cliente_servico_etapa WHERE id = $id_cliente_servico_etapa AND doc_admin IS NOT NULL";
    $res = mysqli_query($conn, $sql);
    $arquivo_cont = mysqli_num_rows($res);

    $sql = "SELECT * FROM documentos_cliente_servico_etapa WHERE id_cliente_servico_etapa = $id_cliente_servico_etapa";
    $res = mysqli_query($conn, $sql);
    $documento_cli_cont = mysqli_num_rows($res);
    if ($documento_cli_cont > 0) {
        while ($row = mysqli_fetch_array($res)) {
            $id_documento = $row['id'];
            $status = $row['status'];
        }
        if ($status == 1) {
            $status = "Em análise";
            $btn_img = "img/analise.png";
            $color = "Orange";
        } else if ($status == 2) {
            $status = "Aprovado";
            $btn_img = "img/aprovado.png";
            $color = "Green";
        }
    }
    ?>
  <div id="etapa2" style="text-align: center;    padding: 0px 20px;">
      <h2>Etapa <?= $num_etapa ?></h2>
      <h4 style="font-size: 18px;" class="servicosButton-Grey">Declaração de Pagamento</h4>
      <h4 style="margin-top: 20px;font-size: 19px;color: #5A5A5A;font-weight: 800;" class="servicosButton-Grey">Declaração de pagamento para assinatura</h4>
      <h4 style="font-size: 13px;" class="servicosButton-Grey">Realize o download de sua declaração
          para pagamento. E a envie após realização da assinatura.</h4>
      <div style="width: 80%;margin-left: 20%;">
          <div class="row" style="align-items: center;text-align: start;">
              <div style="background:#21613A; padding:20px;border-radius: 100px;">
                  <img src="img/cloud.png" alt="">
              </div>
              <div style="margin-left: 15px;">
                  <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                      Declaração:
                      <?php if ($arquivo_cont == 0) { ?>
                          <form id="form-doc-admin" action="php/envia_arquivo_admin.php" method="POST" enctype="multipart/form-data">
                              <input type="hidden" id="id_cliente_servico_etapa" name="id_cliente_servico_etapa" value="<?= $id_cliente_servico_etapa ?>">
                              <input onchange="envia_arquivo()" id="input_file_admin" name="input_file_admin" type="file" style="display:none" accept=".pdf">
                          </form>
                          <button onclick="enviar_documento_admin()" style="border: none;outline: none; font-size: 14px;padding:2px 10px;" class="button-Green"></button>
                  </h4>
                  <a id="arquivo_admin_recente" href="php/download_arquivos_admin.php?id=<?= $id_cliente_servico_etapa ?>" style="color:#21613A; display: none;">declaraçãocliente.pdf</a>
              <?php } else { ?>
                  <button onclick="deleta_documento_admin(<?= $id_cliente_servico_etapa ?>)" style="border: none;outline: none; font-size: 14px;padding:2px 10px;width:auto;border-radius:5px;" class="CancelaUsuario">Excluir</button>
                  </h4>
                  <a href="php/download_arquivos_admin.php?id=<?= $id_cliente_servico_etapa ?>" style="color:#21613A ;">declaraçãocliente.pdf</a>
                  <!-- necessário botão de X para excluir arquivo -->
              <?php } ?>
              </div>
          </div>
          <div class="row" style="align-items: center;text-align: start; margin-top: 10px;">
              <div style="background:#21613A; padding:20px;border-radius: 100px;">
                  <img src="img/clip.png" alt="">
              </div>
              <div style="margin-left: 15px;">
                  <h4 style="font-size: 18px;margin-bottom: 0px;" class="servicosButton-Grey">
                      Documentação:
                      <?php if ($documento_cli_cont > 0) { ?>
                          <a href="php/download_arquivos.php?id=<?= $id_documento ?>&tabela=cliente_servico_etapa" style="outline:none;" class="icon-plusClientes" onclick="">
                              <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                          </a>
                  </h4>
                  <h4 style="font-size: 18px;margin-bottom: 0px;margin-top:10%;" class="servicosButton-<?= $color ?>">
                      <label id="status_doc"><?= $status ?></label>
                      <div class="btn-group dropright">
                          <button id="button_altera_status" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;margin-left:28%;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img id="status_doc_img" style="width: 30px;height:30px;align-self: center; margin-left: 10px;" src="<?= $btn_img ?>">
                          </button>
                          <div class="dropdown-menu" style="min-width: 3.2rem;top: -35px !important;border-radius: 25px;">
                              <div class="triangle-left"></div>
                              <img onclick="altera_status_documento(0,<?= $id_documento ?>,'cliente_servico_etapa')" style="width: 30px;align-self: center; margin-left: 10px; cursor:pointer;" src="img/pendente.png"><br><br>
                              <img onclick="altera_status_documento(1,<?= $id_documento ?>,'cliente_servico_etapa')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/analise.png"><br><br>
                              <img onclick="altera_status_documento(2,<?= $id_documento ?>,'cliente_servico_etapa')" style="width: 30px;align-self: center; margin-left: 10px;cursor:pointer;" src="img/aprovado.png">
                          </div>
                      </div>
                  </h4>

              <?php } else { ?>
                  <a style="outline:none;background: #5A5B5B;cursor:default;" class="icon-plusClientes" onclick="">
                      <i style="color: #AFAFAF" class="fas fa-cloud-download-alt"></i>
                  </a>
                  </h4>
                  <h4 style="font-size: 18px;margin-bottom: 0px;margin-top:10%; color:#FEB548" class="servicosButton-Grey">
                      <label>Pendente</label>
                      <div class="btn-group dropright">
                          <button disabled id="button_altera_status" type="button" class="btn btn-secondary dropdown-toggle" style="background: none;border: none;box-shadow:none;margin-left:28%;">
                              <img id="status_doc_img" style="width: 30px;height:30px;align-self: center; margin-left: 10px;" src="img/pendente.png">
                          </button>
                      </div>
                  </h4>
              <?php } ?>
              </div>
          </div>
      </div>
      <?php if ($status_etapa == 1) { ?>
          <button onclick="finalizar_etapa(<?= $id_cliente_servico ?>,<?= $num_etapa ?>)" class="buttonVoltar">
              Finalizar Etapa
          </button>
      <?php } else if ($status_etapa == 2) { ?>
          <button class="buttonVoltar">
              Etapa Finalizada
          </button>
      <?php } ?>
  </div>
  <!-- ETAPA 02 -->
  <script>
      function enviar_documento_admin() {
          $('#input_file_admin').click();
      }

      function envia_arquivo() {
          $('#form-doc-admin').submit();
          $('#arquivo_admin_recente').css("display", "block");
      }

      function deleta_documento_admin(id) {
          $.get('php/deleta_arquivo_admin.php?id=' + id);
          window.location.href = "index.php#plano-cliente-info.php?id_cliente_aberto="+<?=$id_cliente?>+"&id_cliente_servico="+<?=$id_cliente_servico?>;
          location.reload();
      }

      function altera_status_documento(status, id_documento, tabela) {
          if (status == 0) {
              img = "img/pendente.png";
              msg = "para pendente";
              stt = "Pendente";
              color_stt = "#FEB548";
          } else if (status == 1) {
              img = "img/analise.png";
              msg = "para análise";
              stt = "Em análise";
              color_stt = "#FB6F0A";
          } else if (status == 2) {
              img = "img/aprovado.png";
              msg = "para aprovado";
              stt = "Aprovado";
              color_stt = "#21613A";
          }
          resp = confirm("Deseja realmente alterar o status do documento " + msg + "?");
          if (resp) {
              $.get('php/altera_status_documento.php?status=' + status + '&id_documento=' + id_documento + '&tabela=' + tabela, function(data) {
                  if (data == 1) {
                      $('#status_doc').html(stt);
                      $('#status_doc').css("color", color_stt);
                      document.getElementById('status_doc_img').setAttribute('src', img);
                      if (status == 0) {
                          $('#button_altera_status').attr('disabled', 'disabled');
                      }
                  } else {
                      alert("Não foi possível realizar a alteração.");
                  }
              });
          }
      }
  </script>