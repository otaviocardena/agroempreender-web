<?php
session_start();
include_once("../../conn/conexao.php");

$sql = "SELECT * FROM user_cliente WHERE data_aprovacao IS NOT NULL AND status <> 0";
$res = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">

            <div class="col">
                <div style="display:flex">
                    <h2>Clientes Cadastrados</h2>
                    <button id="buttonCriarNovo" class="btn btn-user btn-block" data-toggle="modal" data-target="#modalCadastroCliente" style="position:inherit; width:auto; padding:0px 17px; margin-left:25px">
                        <i class="fas fa-plus"></i>
                        Criar Novo Cliente
                    </button>
                    <div style="right: 40px;top: 36px;position: absolute;">
                        <input id="pesquisaCliente" type="text" placeholder="Pesquisar cliente...">
                        <i class="fas fa-search botao-pesquisar" onclick="pesquisa_cliente()"></i>
                        </input>
                    </div>
                </div>
                <div>
                    <table id="example" class="table" style="width:100%;margin-top:20px">
                        <thead>
                            <tr>
                                <th style="border-bottom: none; color:#21613A">Nome do Cliente</th>
                                <th style="border-bottom: none;color:#21613A">Cadastrado em</th>
                                <th style="border-bottom: none;color:#21613A">Tipo de Cadastro</th>
                                <th style="border-bottom: none;color:#21613A">Status</th>
                                <th style="border-bottom: none;color:#21613A"></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php while ($row = mysqli_fetch_array($res)) {
                                if ($row['status'] == 1) {
                                    $status = "Ativo";
                                } else if ($row['status'] == 2) {
                                    $status = "Desativado";
                                }
                            ?>
                                <tr>
                                    <td>
                                        <img src="data:image/png;base64,<?= $row['avatar'] ?>" style="width:50px;height:50px;border-radius:100%" alt="">
                                        <?= $row['nome'] ?>
                                    </td>
                                    <td><?= date('d/m/Y', strtotime($row['data_aprovacao'])) ?></td>
                                    <td><?= $row['tipo_cadastro'] ?></td>
                                    <td><?= $status ?></td>
                                    <td>
                                        <button style="outline:none" class="icon-plusClientes" onclick="abrir_cliente(<?= $row['id'] ?>)">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalCadastroCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/cadastra_cliente.php" method="POST">
                    <h2 style="text-align-last: center;">Criar Novo Cliente</h2>
                    <h2 style="text-align-last: center; color:#999999;margin-top: 0px;font-size: 20px;">Insira as informações</h2>
                    <i style="color: #21613A;font-size: 55px;" class="fas fa-user-circle"></i>

                    <div style="text-align: -webkit-center;">
                        <div style="white-space: pre-line;">
                            <input name="nome_cliente" type="text" style="outline:none" class="textCriarCliente" placeholder="Nome Completo" required>
                            <input name="cpf_cliente" id="cpf_cliente" type="text" style="outline:none" class="textCriarCliente" placeholder="CPF" required>
                            <input name="email_cliente" type="text" style="outline:none" class="textCriarCliente" placeholder="E-mail" required>
                            <input name="celular_cliente" type="text" style="outline:none" class="textCriarCliente" placeholder="Celular" required>
                            <input name="login_cliente" type="text" style="outline:none" class="textCriarCliente" placeholder="Login" required>
                            <input name="senha_cliente" type="password" style="outline:none" class="textCriarCliente" placeholder="Senha"  required>
                            <select name="tipo_cad_cliente" style="outline:none" id="tipoCad" required>
                                <option value="Pequeno Produtor">Pequeno Produtor</option>
                                <option value="Medio Produtor">Medio Produtor</option>
                                <option value="Grande Produtor">Grande Produtor</option>
                            </select>
                        </div>

                        <div style="    display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Cadastrar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    $(document).ready(function() {
        $('#cpf_cliente').mask('999.999.999-99');
        return false;
    });

    function abrir_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo").html(data);
        $.get("views/clientes-info.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo').html(data);
        });
    }

    var inputSearch = document.getElementById('pesquisaCliente');
    inputSearch.addEventListener('keyup', function(e) {
        var key = e.which || e.keyCode;
        if (key == 13) {
            $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + this.value, function(data) {
                $('#tbody').html(data);
            });
        }
    });

    function pesquisa_cliente() {
        var pesquisa = $('#pesquisaCliente').val();
        $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + pesquisa, function(data) {
            $('#tbody').html(data);
        });
    }
</script>