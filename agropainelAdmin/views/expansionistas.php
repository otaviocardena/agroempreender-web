<?php
session_start();
include_once("../../conn/conexao.php");

$sql = "SELECT 
            e.id,
            e.nome,
            e.setor,
            p.cidade,
            p.estado 
        FROM expansionista AS e
        INNER JOIN planta AS p ON
            p.id = e.id_planta
        ";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM planta";
$res_plantas = mysqli_query($conn, $sql);
$res_plantas2 = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">

            <div class="col">
                <div style="display:flex">
                    <h2>Expansionistas</h2>
                    <button id="buttonCriarNovo" class="btn btn-user btn-block" data-toggle="modal" data-target="#modalCadastroExpansionista" style="position:inherit; width:auto; padding:0px 17px; margin-left:25px">
                        <i class="fas fa-plus"></i>
                        Criar Novo
                    </button>
                    <!-- <div style="right: 40px;top: 36px;position: absolute;">
                        <input id="pesquisaCliente" type="text" placeholder="Pesquisar cliente...">
                        <i class="fas fa-search botao-pesquisar" onclick="pesquisa_expansionista()"></i>
                        </input>
                    </div> -->
                </div>
                <div>
                    <table id="example" class="table" style="width:100%;margin-top:20px">
                        <thead>
                            <tr>
                                <th style="border-bottom: none; color:#21613A">Nome do Expansionista</th>
                                <th style="border-bottom: none;color:#21613A">Setor</th>
                                <th style="border-bottom: none;color:#21613A">Planta JBS</th>
                                <th width="5%" style="border-bottom: none;color:#21613A"></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php while ($row = mysqli_fetch_array($res)) {
                                $local_planta = $row['cidade'] . " - " . $row['estado']; ?>
                                <tr>
                                    <td><?= $row['nome'] ?></td>
                                    <td><?= $row['setor'] ?></td>
                                    <td><?= $local_planta ?></td>
                                    <td>
                                        <button style="outline:none" class="icon-plusClientes" onclick="edit_expansionista(<?= $row['id'] ?>)">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalCadastroExpansionista" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/cadastra_expansionista.php" method="POST">
                    <h2 style="text-align-last: center;">Criar Novo Expansionista</h2>
                    <h2 style="text-align-last: center; color:#999999;margin-top: 0px;font-size: 20px;">Insira as informações</h2>

                    <div style="margin-top: 20px;">
                        <div class="form-row">
                            <div class="col">
                                <select name="planta_expansionista" id="planta_expansionista" style="outline:none;border: none;border-radius: 20px;height: 35px;color: #686868;font-weight: bold;background: #C4C4C4;width: 100%;">
                                    <?php while ($row = mysqli_fetch_array($res_plantas)) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['cidade'] . " - " . $row['estado'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="setor_expansionista" name="setor_expansionista" style="outline:none" placeholder="Aves - Corte">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="nome_expansionista" name="nome_expansionista" style="outline:none" placeholder="Nome">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="cpf_expansionista" name="cpf_expansionista" style="outline:none" placeholder="Cpf">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="endereco_expansionista" name="endereco_expansionista" style="outline:none" placeholder="Endereço">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="bairro_expansionista" name="bairro_expansionista" style="outline:none" placeholder="Bairro">
                            </div>
                            <div class="col-4">
                                <input type="number" class="textCriarCliente" id="numero_expansionista" name="numero_expansionista" style="outline:none" placeholder="Número">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="complemento_expansionista" name="complemento_expansionista" style="outline:none" placeholder="Endereço">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="cidade_expansionista" name="cidade_expansionista" style="outline:none" placeholder="Bairro">
                            </div>
                            <div class="col-4">
                                <select name="estado_expansionista" id="estado_expansionista" style="outline:none;border: none;border-radius: 20px;height: 35px;color: #686868;font-weight: bold;background: #C4C4C4;width: 100%;">
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>
                        </div>
                        <div style="display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Cadastrar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEditExpansionista" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/edita_expansionista.php" method="POST">
                <input type="hidden" id="id_expansionista_edit" name="id_expansionista_edit">
                    <h2 style="text-align-last: center;">Editar Expansionista</h2>
                    <h2 style="text-align-last: center; color:#999999;margin-top: 0px;font-size: 20px;">Insira as informações</h2>

                    <div style="margin-top: 20px;">
                        <div class="form-row">
                            <div class="col">
                                <select name="planta_expansionista_edit" id="planta_expansionista_edit" style="outline:none;border: none;border-radius: 20px;height: 35px;color: #686868;font-weight: bold;background: #C4C4C4;width: 100%;">
                                    <?php while ($row = mysqli_fetch_array($res_plantas2)) { ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['cidade'] . " - " . $row['estado'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="setor_expansionista_edit" name="setor_expansionista_edit" style="outline:none" placeholder="Aves - Corte">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="nome_expansionista_edit" name="nome_expansionista_edit" style="outline:none" placeholder="Nome">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="cpf_expansionista_edit" name="cpf_expansionista_edit" style="outline:none" placeholder="Cpf">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="endereco_expansionista_edit" name="endereco_expansionista_edit" style="outline:none" placeholder="Endereço">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="bairro_expansionista_edit" name="bairro_expansionista_edit" style="outline:none" placeholder="Bairro">
                            </div>
                            <div class="col-4">
                                <input type="number" class="textCriarCliente" id="numero_expansionista_edit" name="numero_expansionista_edit" style="outline:none" placeholder="Número">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="complemento_expansionista_edit" name="complemento_expansionista_edit" style="outline:none" placeholder="Endereço">
                            </div>
                            <div class="col">
                                <input type="text" class="textCriarCliente" id="cidade_expansionista_edit" name="cidade_expansionista_edit" style="outline:none" placeholder="Bairro">
                            </div>
                            <div class="col-4">
                                <select name="estado_expansionista_edit" id="estado_expansionista_edit" style="outline:none;border: none;border-radius: 20px;height: 35px;color: #686868;font-weight: bold;background: #C4C4C4;width: 100%;">
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>
                        </div>
                        <div style="display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Editar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        // $('#example').DataTable();
        $('#cpf_expansionista').mask('999.999.999-99');
        $('#cpf_expansionista_edit').mask('999.999.999-99');
        // return false;
    });

    function edit_expansionista(id) {
        $.get("php/get_expansionistas.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $('#id_expansionista_edit').val(id);
            $('#planta_expansionista_edit').val(json[0].id_planta);
            $('#nome_expansionista_edit').val(json[1].nome);
            $('#cpf_expansionista_edit').val(json[2].cpf);
            $('#cidade_expansionista_edit').val(json[3].cidade);
            $('#endereco_expansionista_edit').val(json[4].endereco);
            $('#numero_expansionista_edit').val(json[5].numero);
            $('#bairro_expansionista_edit').val(json[6].bairro);
            $('#complemento_expansionista_edit').val(json[7].complemento);
            $('#estado_expansionista_edit').val(json[8].estado);
            $('#setor_expansionista_edit').val(json[9].setor);



            $('#modalEditExpansionista').modal("show");
        });
    }

    // var inputSearch = document.getElementById('pesquisaCliente');
    // inputSearch.addEventListener('keyup', function(e) {
    //     var key = e.which || e.keyCode;
    //     if (key == 13) {
    //         $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + this.value, function(data) {
    //             $('#tbody').html(data);
    //         });
    //     }
    // });

    // function pesquisa_expansionista() {
    //     var pesquisa = $('#pesquisaCliente').val();
    //     $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + pesquisa, function(data) {
    //         $('#tbody').html(data);
    //     });
    // }
</script>