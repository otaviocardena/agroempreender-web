<?php
include_once("../../conn/conexao.php");
$sql = "SELECT * FROM user_admin";
$res_clientes = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">
            <div class="col-xl-5 col-lg-5">
                <div style="display:flex">
                    <h2>Usuários</h2>
                    <button id="buttonCriarNovo" class="btn btn-user btn-block" onclick="abrir_novo_usuario()">
                        <i class="fas fa-plus"></i>
                        Criar Novo
                    </button>
                </div>

                <div id="accordion1" style="flex-direction:column; height:70vh;margin: 20px 0px;">
                    <?php while ($row = mysqli_fetch_array($res_clientes)) { ?>
                        <div class="users" onclick="abrir_usuario(<?= $row['id'] ?>)">
                            <img class="rounded-circle" style="height:80px;width:80px" src="data:image/png;base64,<?= $row['avatar'] ?>"" alt="">
                            <div style=" margin-left:10px">
                            <h5 class="name-users"><?= $row['nome'] ?></h5>
                            <h5 class="info-users"><?= $row['email'] ?></h5>
                            <h5 class="info-users"s><?= $row['celular'] ?></h5>
                            <h5 class="info-users"><?= $row['cpf'] ?></h5>
                        </div>
                </div>
            <?php } ?>
            </div>

        </div>



        <div class="col">
            <div class="criar-novo" id="conteudo-user" style="background:#ECECEC;max-height:100%;margin-top: 10%;border-radius: 15px;padding:6.5% 30px;">
                <div style="margin-top:20%;">
                    <div style="text-align: -webkit-center;padding: 0px 20px;">
                        <div style="width: fit-content;">
                            <img src="img/question.png" alt="">
                        </div>
                        <h2 style="margin-top:10px">Usuários</h2>
                        <h4 style="font-size: 18px;" class="servicosButton-Grey">Consulte sua equipe, gerencie suas funções, permissões. Crie novos usuários.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function abrir_novo_usuario() {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-user").html(data);
        $.get("views/subviews/usuarios-cadastro.php", function(data) {
            $('#conteudo-user').html(data);
        });
    }

    function abrir_usuario(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-user").html(data);
        $.get("views/subviews/user_info.php?id_user_aberto=" + id, function(data) {
            $('#conteudo-user').html(data);
        });
    }

    function altera_icone(i) {
        if (document.getElementById("toggle" + i)) {
            if (document.getElementById("toggle" + i).checked == true) {
                $('#icon' + i).removeClass('fa-plus').addClass('fa-minus');
            } else {
                $('#icon' + i).removeClass('fa-minus').addClass('fa-plus');
            }
        }

    }

    function cancela_alteracoes() {
        //     $('#login_user').val('$login');
        //     $('#senha_user').val(' $senha ');
        //     $('#email_user').val(' $email ');
        //     $('#nome_user').val(' $nome ');
        //     $('#cpf_user').val(' $cpf ');
        //     $('#celular_user').val(' $celular ');

        //     for (i = 1; i <= 5; i++) {
        //         $('#toggle' + i).removeAttr('checked');
        //         $('#icon' + i).removeClass('fa-minus').addClass('fa-plus');
        //     }

        //     php
        //     foreach ($permissions_user_aberto as $key => $value) {
        //         if (in_array($value, $permissions_user_aberto)) {
        //     
        //             $('#toggle' + = $value ).attr('checked', 'checked');
        //             if (document.getElementById("toggle" + = $value ).checked == true) {
        //                 $('#icon' + = $value ).removeClass('fa-plus').addClass('fa-minus');
        //             } else {
        //                 $('#icon' + = $value ).removeClass('fa-minus').addClass('fa-plus');
        //             }
        //     php
        //         }
        //     }
        location.reload();
    }
</script>