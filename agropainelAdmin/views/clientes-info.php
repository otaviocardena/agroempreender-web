<?php
session_start();
include_once("../../conn/conexao.php");
$id_cliente_aberto = $_GET['id_cliente_aberto'];

$sql = "SELECT * FROM user_cliente WHERE id = $id_cliente_aberto";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $avatar = $row['avatar'];
    $nome = $row['nome'];
    $data_aprovacao = $row['data_aprovacao'];
    $status = $row['status'];
    $tipo_cadastro = $row['tipo_cadastro'];
    $limite = $row['limite_credito'];
    $cpf = $row['cpf'];
    $email = $row['email'];
    $telefone = $row['telefone'] ? $row['telefone'] : "Não há informação";
    $celular = $row['celular'] ? $row['celular'] : "Não há informação";
}

$sql = "SELECT * FROM enderecos_cliente WHERE id_cliente = $id_cliente_aberto";
$res = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row" style="margin: 0px 20px 0px 0px;min-height: 80vh;">

            <div class="col-xl-3 col-lg-3 col-md-6 shadow" id="cardLateral" style="border-radius: 30px;padding:0px">
                <div class="form-row" style="margin-top: 25px;justify-content: center;">
                    <img class="rounded-circle" style="height:150px;width:150px" src="data:image/png;base64,<?= $avatar ?>" alt="">
                </div>
                <div class="col" style="margin-top: 10px;text-align:center;">
                    <h2 style="font-size:22px"><?= $nome ?></h2>
                    <p class="descOfPlan" style="margin-bottom: 0px;font-weight: bold;">Limite de Crédito:
                        <span style="color:#21613A">R$</span>
                        <input autofocus style="outline-color:#21613A   " id="valorCredito" type="number" step="0.01" value="<?= $limite ?>" disabled></input>
                        <button style="background: #21613A;border: none;border-radius: 40%;outline: none;" onclick="habilitarEditCredito(<?= $id_cliente_aberto ?>)">
                            <i class="fas fa-pen" style="color: #F6D838;"></i>
                        </button>

                    </p>
                    <p class="descOfPlan" style="margin-bottom: 0px;font-weight: bold;">Data de Cadastro: <?= date('d/m/Y', strtotime($data_aprovacao)) ?></p>
                    <div class="dropdown">
                        <button style="outline:none;background: none;color: #858799;font-size: 16px;font-weight: bold;border: none;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span id="tipo-cadastro-span"><?= $tipo_cadastro ?></span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button class="dropdown-item" onclick="altera_tipo_cadastro_cliente(<?= $id_cliente_aberto ?>,'Pequeno Produtor')">Pequeno Produtor</button>
                            <button class="dropdown-item" onclick="altera_tipo_cadastro_cliente(<?= $id_cliente_aberto ?>,'Medio Produtor')">Medio Produtor</button>
                            <button class="dropdown-item" onclick="altera_tipo_cadastro_cliente(<?= $id_cliente_aberto ?>,'Grande Produtor')">Grande Produtor</button>
                        </div>
                    </div>
                </div>
                <div class="col" style="margin-top: 20px;padding:0px">
                    <button class="buttonInfoCliente" style="    outline: none;" onclick="abrir_dados_cliente(<?= $id_cliente_aberto ?>)">
                        <i style="font-size:25px" class="fas fa-fingerprint"></i>
                        <p style="margin-left:5px;margin-bottom: 0px;">Dados do Cliente</p>
                        <div class="hoverActiveInfo"></div>
                    </button>
                </div>
                <div class="col" style="margin-top: 20px;padding:0px">
                    <button class="buttonInfoCliente" style="    outline: none;" onclick="abrir_plano_cliente(<?= $id_cliente_aberto ?>)">
                        <i style="font-size:25px" class="far fa-bookmark"></i>
                        <p style="margin-left:5px;margin-bottom: 0px;">Planos de Cliente</p>
                        <div class="hoverActiveInfo"></div>
                    </button>
                </div>
                <?php if (in_array(2, $_SESSION['permissionsAdm'])) { ?>
                    <div class="col" style="margin-top: 20px;padding:0px">
                        <button class="buttonInfoCliente" style="    outline: none;" onclick="abrir_documento_cliente(<?= $id_cliente_aberto ?>)">
                            <i style="font-size:25px" class="far fa-folder-open"></i>
                            <p style="margin-left:5px;margin-bottom: 0px;">Documentação</p>
                            <div class="hoverActiveInfo"></div>
                        </button>
                    </div>
                <?php } ?>
                <?php if ($status == 1) { ?>
                    <div class="col" style="margin-top: 20px;padding:0px;position: absolute;bottom: 15px;text-align:center;">
                        <button id="buttonDesativar" class="buttonInfoCliente" style="width:auto;outline: none;" onclick="altera_status_cliente(<?= $id_cliente_aberto ?>,2)">
                            <i style="font-size:25px" class="fas fa-power-off"></i>
                            <p style="margin-left:5px;margin-bottom: 0px;">Desativar</p>
                        </button>
                    </div>
                <?php } else if ($status == 2) { ?>
                    <div class="col" style="margin-top: 20px;padding:0px;position: absolute;bottom: 15px;text-align:center;">
                        <button class="buttonInfoCliente" style="width:auto;outline: none;" onclick="altera_status_cliente(<?= $id_cliente_aberto ?>,1)">
                            <i style="font-size:25px" class="fas fa-power-off"></i>
                            <p style="margin-left:5px;margin-bottom: 0px;">Ativar</p>
                        </button>
                    </div>
                <?php } ?>
            </div>
            <div class="col-8" id="conteudo-subview">
                <div class="col">
                    <div class="form-row">
                        <h2>Informações Pessoais</h2>
                    </div>
                    <div id="accordion1" style="height:70vh;margin: 20px 0px;overflow-x:hidden;">
                        <div>
                            <p class="info-ClientesText">CPF: <span style="color:#686868"><?= $cpf ?></span></p>
                            <p class="info-ClientesText">E-mail: <span style="color:#686868"><?= $email ?></span></p>
                            <p class="info-ClientesText">Telefone: <span style="color:#686868"><?= $telefone ?></span></p>
                            <p class="info-ClientesText">Celular: <span style="color:#686868"><?= $celular ?></span></p>
                        </div>
                        <div class="form-row">
                            <h2 style="margin-left: 2px;">Endereços</h2>
                        </div>
                        <div>
                            <?php foreach ($res as $key => $value) { ?>
                                <div class="buttonEndereco" style="outline:none; display:flex;margin-top:10px; width:100%;">
                                    <div style="border-radius: 100%;place-self: center;font-size: 35px;background: #21613A;padding: 15px 20px; margin-right:10px">
                                        <i style="color:#F6D838" class="fas fa-home"></i>
                                    </div>
                                    <div style="display:block;text-align: start;margin-right: 15px;">
                                        <p class="info-EnderecoCliente">Endereço <?= $key + 1 ?></p>
                                        <p class="info-EnderecoCliente1"><?= $value['endereco'] . ", " . $value['numero'] . ", " . $value['bairro'] ?></p>
                                        <p class="info-EnderecoCliente1"><?= $value['cidade'] . ", " . $value['estado'] ?></p>
                                        <p class="info-EnderecoCliente1"><?= $value['cep'] ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<script>
    function abrir_dados_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/subviews/dados-cliente.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function abrir_plano_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/subviews/plano-cliente.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function abrir_documento_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/subviews/documentos-cliente.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function altera_status_cliente(id, status) {
        $.get("php/altera_status_cliente.php?id_cliente_aberto=" + id + "&status=" + status);
        window.location.href = "index.php#clientes-cadastrados";
        location.reload();
    }

    function altera_tipo_cadastro_cliente(id, tipo) {
        $.get("php/altera_tipo_cadastro_cliente.php?id_cliente_aberto=" + id + "&tipo=" + tipo);
        $('#tipo-cadastro-span').html(tipo);
    }

    function habilitarEditCredito(id) {
        var inputCredito = document.getElementById("valorCredito")
        if (inputCredito.disabled) {
            inputCredito.disabled = false;
            inputCredito.focus();
        } else {
            inputCredito.disabled = true;
            var credito = inputCredito.value;
            $.get("php/altera_credito_cliente.php?id_cliente_aberto=" + id + "&credito=" + credito);
        }
    }
</script>