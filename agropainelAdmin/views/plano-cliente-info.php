<?php
include_once("../../conn/conexao.php");
session_start();


$id_cliente_aberto = $_GET['id_cliente_aberto'];
$id_cliente_servico = $_GET['id_cliente_servico'];

$sql = "SELECT avatar FROM user_cliente WHERE id = $id_cliente_aberto";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $avatar = $row[0];
}

$sql = "SELECT 
            s.nome
        FROM cliente_servico as cs
        INNER JOIN servicos as s
            ON s.id = cs.id_servico
        WHERE cs.id = $id_cliente_servico";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $nome_servico = $row[0];
}

$sql = "SELECT
            cs.id as id_cliente_servico,
            cs.data_cad,
            cse.etapa,
            cse.status as status_etapa,
            e.id as id_etapa,
            e.nome as nome_etapa
        FROM cliente_servico AS cs
        INNER JOIN cliente_servico_etapa as cse
            ON cs.id = cse.id_cliente_servico
        INNER JOIN servicos AS s 
            ON cs.id_servico = s.id
        INNER JOIN etapa_servico AS es
            ON cs.id_servico = es.id_servico AND
            cse.etapa = es.etapa
        INNER JOIN etapas as e
            ON es.id_etapa = e.id
        WHERE
            cs.id = $id_cliente_servico
        ";
$res = mysqli_query($conn, $sql);

?>
<div class="container-fluid">
    <div class="card">
        <div class="row" style="margin: 0px 0px 0px 0px;min-height: 80vh;">
            <div class="shadow" id="cardLateral" style="border-radius: 30px;padding:0px; width:100px;position:relative">
                <div class="form-row" style="margin-top: 25px;justify-content: center;">
                    <img class="rounded-circle" style="height:50px;width:50px" src="data:image/png;base64,<?= $avatar ?>" alt="">
                </div>
                <div class="col" style="margin-top: 20px;padding:0px;position: absolute;bottom: 25px;">
                    <button class="buttonInfoCliente" style="    outline: none;" onclick="abrir_cliente(<?= $id_cliente_aberto ?>)">
                        <i style="font-size:25px" class="fas fa-arrow-left"></i>
                        <div class="hoverActiveInfo"></div>
                    </button>
                </div>
            </div>
            <div class="row" style="margin: 10px 20px 20px 20px; width:86%">
                <div class="col-5">
                    <div style="display:flex">
                        <h2><?= $nome_servico ?></h2>
                    </div>
                    <div id="accordion1" style="height:70vh;margin: 20px 0px;    padding: 0px 10px 0px 0px;">
                        <?php while ($row = mysqli_fetch_array($res)) {
                            if ($row['status_etapa'] == 0) {
                                $status = "Fechado";
                                $color_st = "#c62121";
                                $icon_st = "lock";
                            } else if ($row['status_etapa'] == 1) {
                                $status = "Em Andamento";
                                $color_st = "#feb548";
                                $icon_st = "unlock";
                            } else if ($row['status_etapa'] == 2) {
                                $status = "Aprovado";
                                $color_st = "#08d659";
                                $icon_st = "unlock";
                            } ?>
                            <div class="buttonServico" style="display:flex; margin-top:10px;position:relative; cursor:pointer;" onclick="abrir_etapa(<?= $row['id_etapa'] ?>,<?= $id_cliente_servico ?>,<?= $row['etapa'] ?>)">
                                <div id="div-icon-lock-<?= $row['etapa'] ?>" class="icon-lockEtapa" style="cursor: pointer;" onclick="liberar_etapa(<?= $row['id_cliente_servico'] ?>,<?= $row['status_etapa'] ?>,<?= $row['etapa'] ?>)">
                                    <!-- <i class="fas fa-lock"></i> -->
                                    <i id="icone-liberacao-<?= $row['etapa'] ?>" class="fas fa-<?= $icon_st ?>"></i>
                                </div>
                                <div class="insideButton" style="text-align-last: center;">
                                    <div style=" width: 108px;   margin-top: 10px;">
                                        <img src="img/document.png" alt="">
                                    </div>

                                </div>
                                <div style="text-align: start;padding: 10px;">
                                    <h4 class="servicosButton-Green" style="cursor:pointer" onclick="abrir_etapa(<?= $row['id_etapa'] ?>,<?= $id_cliente_servico ?>,<?= $row['etapa'] ?>)">Etapa <?= $row['etapa'] ?></h4>
                                    <h4 class="servicosButton-Grey nome-etapa"><?= $row['nome_etapa'] ?></h4>
                                    <h4 class="servicosButton-Grey">Status: <span id="status-etapa-<?= $row['etapa'] ?>" style="color:<?= $color_st ?>"><?= $status ?></span></h4>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col">
                    <div class="card2" style="place-content: center;">
                        <div id="conteudo-subview"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<script>
    function abrir_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo").html(data);
        $.get("views/clientes-info.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo').html(data);
        });
    }

    function abrir_etapa(etapa, id_cli_serv, num_etapa) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/etapas/etapa" + etapa + ".php?id_cliente_servico=" + id_cli_serv + "&num_etapa=" + num_etapa, function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function liberar_etapa(id_cli_serv, status, etapa) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();

        if (status == 0) {
            resp = confirm("Deseja liberar a Etapa " + etapa + "?");
            if (resp) {
                $.get("php/libera_etapa.php?id_cliente_servico=" + id_cli_serv + "&etapa=" + etapa);

                $('#icone-liberacao-' + etapa).removeClass('fas fa-lock').addClass('fas fa-unlock');
                $('#status-etapa-' + etapa).css('color', '#feb548');
                $('#status-etapa-' + etapa).html('Em Andamento');
                $('#div-icon-lock-' + etapa).removeAttr('onclick');
            }
        }
    }
</script>