<div class="container-fluid">
  <div class="card card-clientes" style="margin: 10px;">
    <div class="row background-todos">
      <div class="col">
        <div style="text-align: -webkit-center;">
          <h2>Qual opção deseja acessar?</h2>
          <p class="descOfPlan" style="font-weight: bold;">Consulte ou edite os processos dos seus serviços.</p>
          <div class="card-clientes">
            <div class="button-clientes" onclick="page('plantas')">
              <div style="height:100%;padding: 85px 10px;">
                <img src="./img/jbs.png" alt="">
                <h3 style="font-weight: bold;color:#F6D838;">Planta JBS</h3>
                <h5 style="color:#fff; font-size:12px">Consulte as Plantas JBS disponíveis para uso</h5>
              </div>

            </div>
            <div class="button-clientes" onclick="page('expansionistas')">
              <div style="height:100%;padding: 85px 10px;">
                <img src="./img/expansionista.png" style="height:49px;">
                <h3 style="font-weight: bold;color:#F6D838;">Expansionista</h3>
                <h5 style="color:#fff; font-size:12px">Realize o cadastro de expansionistas</h5>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>