<?php
session_start();
include_once("../../conn/conexao.php");

$sql = "SELECT 
            cs.id AS id_cliente_servico,
            cs.id_servico AS id_servico,
            cs.id_cliente AS id_cliente_aberto,
            uc.nome AS nome_cliente,
            uc.avatar AS foto_cliente,
            s.nome AS nome_servico,
            cs.data_cad,
            cse.etapa
        FROM cliente_servico AS cs 
        INNER JOIN cliente_servico_etapa AS cse
            ON cs.id = cse.id_cliente_servico
        INNER JOIN servicos AS s 
            ON cs.id_servico = s.id
        INNER JOIN user_cliente AS uc
            ON cs.id_cliente = uc.id
        WHERE cse.status = 1 OR cse.status = 0 
        GROUP BY 
            cse.id_cliente_servico";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM servicos";
$res_servicos = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">

            <div class="col">
                <div style="display:flex">
                    <h2>Serviços em Andamento</h2>
                    <button id="buttonCriarNovo" class="btn btn-user btn-block" data-toggle="modal" data-target="#modalCadastroNovoServico" style="position:inherit; width:auto; padding:0px 17px; margin-left:25px">
                        <i class="fas fa-plus"></i>
                        Criar Novo
                    </button>
                    <div style="right: 40px;top: 36px;position: absolute;">
                        <input id="pesquisaServico" type="text" placeholder="Pesquisar serviço...">
                        <i style="cursor: pointer;position: absolute;right: 10px;top: 8px;" class="fas fa-search" onclick="pesquisa_cliente()"></i>
                        </input>
                    </div>
                </div>
                <div>
                    <table id="example" class="table" style="width:100%;margin-top:20px">
                        <thead>
                            <tr>
                                <th style="border-bottom: none; color:#21613A">Nome do Cliente</th>
                                <th style="border-bottom: none;color:#21613A">Cadastrado em</th>
                                <th style="border-bottom: none;color:#21613A">Tipo de Serviço</th>
                                <th style="border-bottom: none;color:#21613A">Etapa Atual</th>
                                <th style="border-bottom: none;color:#21613A"></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php while ($row = mysqli_fetch_array($res)) { ?>
                                <tr>
                                    <td>
                                        <img src="data:image/png;base64,<?= $row['foto_cliente'] ?>" style="width:50px;height:50px;border-radius:100%" alt="">
                                        <?= $row['nome_cliente'] ?>
                                    </td>
                                    <td><?= date('d/m/Y', strtotime($row['data_cad'])) ?></td>
                                    <td><?= $row['nome_servico'] ?></td>
                                    <td><?= "Etapa " . $row['etapa'] ?></td>
                                    <td>
                                        <button style="outline:none" class="icon-plusClientes" onclick="abrir_plano_info(<?= $row['id_cliente_aberto'] ?>,<?= $row['id_cliente_servico'] ?>)">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalCadastroNovoServico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 60%;">
        <div class="modal-content" style="background-color: #ECECEC;border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/cadastra_servico.php" method="POST" id="form-cadastra-servico">
                    <h2 style="text-align-last: center; font-size: 22px;">Criar Novo Serviço</h2>

                    <div style="text-align: -webkit-center;">
                        <div style="white-space: pre-line;">
                            <div class="row">
                                <div class="col-8">
                                    <h3 class="textoSuperior">Tipo de serviço:</h3>
                                    <select name="servico" style="width: 100%;outline:none; background-color: #fff;" id="tipoCad" required>
                                        <?php while ($row = mysqli_fetch_array($res_servicos)) { ?>
                                            <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <h3 class="textoSuperior">Estado:</h3>
                                    <select name="estado" style="width: 100%;outline:none;background-color: #fff" id="tipoCad" required>
                                        <option value="AC">Acre</option>
                                        <option value="AL">Alagoas</option>
                                        <option value="AP">Amapá</option>
                                        <option value="AM">Amazonas</option>
                                        <option value="BA">Bahia</option>
                                        <option value="CE">Ceará</option>
                                        <option value="DF">Distrito Federal</option>
                                        <option value="ES">Espírito Santo</option>
                                        <option value="GO">Goiás</option>
                                        <option value="MA">Maranhão</option>
                                        <option value="MT">Mato Grosso</option>
                                        <option value="MS">Mato Grosso do Sul</option>
                                        <option value="MG">Minas Gerais</option>
                                        <option value="PA">Pará</option>
                                        <option value="PB">Paraíba</option>
                                        <option value="PR">Paraná</option>
                                        <option value="PE">Pernambuco</option>
                                        <option value="PI">Piauí</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="RN">Rio Grande do Norte</option>
                                        <option value="RS">Rio Grande do Sul</option>
                                        <option value="RO">Rondônia</option>
                                        <option value="RR">Roraima</option>
                                        <option value="SC">Santa Catarina</option>
                                        <option value="SP">São Paulo</option>
                                        <option value="SE">Sergipe</option>
                                        <option value="TO">Tocantins</option>
                                    </select>
                                </div>
                            </div>
                            <h2 style="font-size: 1.55rem;">Participantes</h2>
                            <div style="text-align: center; padding: 0px 11vh; margin-top: -30px;">
                                <div style="display: flex;">
                                    <div onclick="libera_input(1)" style="font-size: 17px;background-color: #21613A;border-radius: 50%;margin: 0px 10px;padding: 5px 13px 5px 13px;font-weight: 700;color: yellow;border: none;"> 1</div>
                                    <input name="cpf_cliente_1" id="cpf_cliente_1" type="text" style="outline:none" class="textCriarCliente1" placeholder="CPF" required>
                                </div>
                                <div style="display: flex;">
                                    <div onclick="libera_input(2)" style="font-size: 17px;background-color: #21613A;border-radius: 50%;margin: 0px 10px;padding: 5px 13px 5px 13px;font-weight: 700;color: yellow;border: none;"> 2</div>
                                    <input name="cpf_cliente_2" id="cpf_cliente_2" type="text" style="outline:none" class="textCriarCliente1" placeholder="CPF" readonly>
                                </div>
                                <div style="display: flex;">
                                    <div onclick="libera_input(3)" style="font-size: 17px;background-color: #21613A;border-radius: 50%;margin: 0px 10px;padding: 5px 13px 5px 13px;font-weight: 700;color: yellow;border: none;"> 3</div>
                                    <input name="cpf_cliente_3" id="cpf_cliente_3" type="text" style="outline:none" class="textCriarCliente1" placeholder="CPF" readonly>
                                </div>
                            </div>
                        </div>

                        <div style="display: flex;place-content: center;justify-content: space-between;">
                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="button" onclick="cadastrar_servico()" class="CadUsuario btn btn-user btn-block">
                                Confirmar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    $(document).ready(function() {
        $('#cpf_cliente_1').mask('999.999.999-99');
        $('#cpf_cliente_2').mask('999.999.999-99');
        $('#cpf_cliente_3').mask('999.999.999-99');
        return false;
    });

    function libera_input(i){
        $('#cpf_cliente_'+i).removeAttr("readonly");
        $('#cpf_cliente_'+i).focus();
    }


    function abrir_cliente(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo").html(data);
        $.get("views/clientes-info.php?id_cliente_aberto=" + id, function(data) {
            $('#conteudo').html(data);
        });
    }

    var inputSearch = document.getElementById('pesquisaServico');
    inputSearch.addEventListener('keyup', function(e) {
        var key = e.which || e.keyCode;
        if (key == 13) {
            $.get("views/subviews/servicos-pesquisa.php?pesquisa=" + this.value, function(data) {
                $('#tbody').html(data);
            });
        }
    });

    function pesquisa_cliente() {
        var pesquisa = $('#pesquisaServico').val();
        $.get("views/subviews/servicos-pesquisa.php?pesquisa=" + pesquisa, function(data) {
            $('#tbody').html(data);
        });
    }

    function abrir_plano_info(id_cli, id_cli_serv) {
        var id = $('#id_cliente_plano').val();
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo").html(data);
        $.get("views/plano-cliente-info.php?id_cliente_aberto=" + id_cli + "&id_cliente_servico=" + id_cli_serv, function(data) {
            $('#conteudo').html(data);
        });
    }
    
    function cadastrar_servico(){
        $('#form-cadastra-servico').submit();
    }
</script>