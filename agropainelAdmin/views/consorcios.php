<?php
include_once("../../conn/conexao.php");
$sql = "SELECT * FROM consorcios WHERE status = 0";
$res_inativos = mysqli_query($conn, $sql);

$sql = "SELECT * FROM consorcios WHERE status = 1";
$res_ativos = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row" style="margin: 10px 20px 20px 20px;">
            <div style="display:flex;width: 100%;">
                <button id="buttonCriarNovo1" class="btn btn-user btn-block" onclick="abrir_consorcio_novo()">
                    <i class="fas fa-plus"></i>
                    Criar Novo
                </button>
                <select style="outline:none;margin-top: 28px;margin-left:20px; width:200px" id="tipoCad" required>
                    <option value="CADESP">Ordernar</option>
                </select>
            </div>
            <div class="form-row" style="width:100%">
                <div class="col-6">
                    <div class="form-row">
                        <div style="display:flex;margin-left: 20px;">
                            <h2>Ativos</h2>
                        </div>
                        <div id="accordion">
                            <div class="row" style="flex-wrap: nowrap;padding: 20px;">
                                <?php while ($row = mysqli_fetch_array($res_ativos)) { ?>
                                    <div class=" button-telaServicos col-4" style="margin-right:10px;height:200px" onclick="abrir_consorcio_editar(<?= $row['id'] ?>)">
                                        <span class="span-editConsorcio">
                                            <img src="./img/editar.png" alt="">
                                        </span>
                                        <div style="padding: 65px 10px;text-align: -webkit-center;">
                                            <div style="display:flex">
                                                <img src="data:image/png;base64,<?= $row['icone'] ?>" style="width:50px" alt="">
                                                <h3 style="font-weight: bold;color: #F6D838;align-self: center;font-size: 18px;margin-bottom: 0px;margin-left: 5px;"><?= $row['titulo'] ?></h3>
                                            </div>

                                            <h5 style="color:#fff; font-size:12px"><?= $row['subtitulo'] ?></h5>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div style="display:flex;margin-left: 20px;">
                            <h2>Inativos</h2>
                        </div>
                        <div id="accordion">
                            <div class="row" style="flex-wrap: nowrap;padding: 20px;">
                                <?php while ($row = mysqli_fetch_array($res_inativos)) { ?>
                                    <div class=" button-telaServicos col-4" style="margin-right:10px;height:200px" onclick="abrir_consorcio_editar(<?= $row['id'] ?>)">
                                        <span class="span-editConsorcio">
                                            <img src="./img/editar.png" alt="">
                                        </span>
                                        <div style="padding: 65px 10px;text-align: -webkit-center;">
                                            <div style="display:flex">
                                                <img src="data:image/png;base64,<?= $row['icone'] ?>" style="width:50px" alt="">
                                                <h3 style="font-weight: bold;color: #F6D838;align-self: center;font-size: 18px;margin-bottom: 0px;margin-left: 5px;"><?= $row['titulo'] ?></h3>
                                            </div>

                                            <h5 style="color:#fff; font-size:12px"><?= $row['subtitulo'] ?></h5>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="conteudo-subview" class="col-6" style="background:#EDEDED; border-radius:25px;    padding-right: 20px;">
                    <div style="margin-top:35%;">
                        <div style="text-align: -webkit-center;padding: 0px 20px;">
                            <h2 style="margin-top:10px">Consórcios</h2>
                            <h4 style="font-size: 18px;margin-bottom:0" class="servicosButton-Grey">Crie novos consórcios, edite já existentes.</h4>
                            <h4 style="font-size: 18px;" class="servicosButton-Grey">Publique ou exclua de manteira simples.</h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    function abrir_consorcio_novo() {
        var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/subviews/consorcio-cadastro.php", function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function mostrarimg(obj, tipo) {
        console.log("entrou");
        var file = document.getElementById(obj.id).files;

        if (file.length > 0) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if (tipo == 1) {
                    document.getElementById('icone-show').setAttribute("src", e.target.result);
                } else if (tipo == 2) {
                    document.getElementById('img-show').setAttribute("src", e.target.result);
                }
            }

            reader.readAsDataURL(file[0]);
        }
    }

    function abrir_consorcio_editar(id) {
        var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo-subview").html(data);
        $.get("views/subviews/consorcio-editar.php?id_consorcio_aberto="+id, function(data) {
            $('#conteudo-subview').html(data);
        });
    }

    function altera_icone() {
        if (document.getElementById("toggle4")) {
            if (document.getElementById("toggle4").checked == true) {
                $('#icon4').removeClass('fa-plus').addClass('fa-minus');
                $('#txt_status').html("Inativo");
            } else {
                $('#icon4').removeClass('fa-minus').addClass('fa-plus');
                $('#txt_status').html("Ativo");
            }
        }
    }

    function abrir_input(opc) {
        if (opc == 1) {
            $("#icone_consorcio").click();
        } else if (opc == 2) {
            $("#img_consorcio").click();
        }
    }
</script>