<?php
session_start();
include_once("../../conn/conexao.php");

$sql = "SELECT * FROM planta";
$res = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">

            <div class="col">
                <div style="display:flex">
                    <h2>Plantas</h2>
                    <button id="buttonCriarNovo" class="btn btn-user btn-block" data-toggle="modal" data-target="#modalCadastroPlanta" style="position:inherit; width:auto; padding:0px 17px; margin-left:25px">
                        <i class="fas fa-plus"></i>
                        Criar Novo
                    </button>
                    <!-- <div style="right: 40px;top: 36px;position: absolute;">
                        <input id="pesquisaCliente" type="text" placeholder="Pesquisar cliente...">
                        <i class="fas fa-search botao-pesquisar" onclick="pesquisa_expansionista()"></i>
                        </input>
                    </div> -->
                </div>
                <div>
                    <table id="example" class="table" style="width:100%;margin-top:20px">
                        <thead>
                            <tr>
                                <th style="border-bottom: none; color:#21613A">Cidade</th>
                                <th style="border-bottom: none;color:#21613A">UF</th>
                                <th width="5%" style="border-bottom: none;color:#21613A"></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php while ($row = mysqli_fetch_array($res)) { ?>
                                <tr>
                                    <td><?= $row['cidade'] ?></td>
                                    <td><?= $row['estado'] ?></td>
                                    <td>
                                        <button style="outline:none" class="icon-plusClientes" onclick="edit_planta(<?= $row['id'] ?>)">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalCadastroPlanta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/cadastra_planta.php" method="POST">
                    <h2 style="text-align-last: center;">Criar Nova Planta</h2>
                    <h2 style="text-align-last: center; color:#999999;margin-top: 0px;font-size: 20px;">Insira as informações</h2>

                    <div style="margin-top:20px">
                        <div class="form-row">
                            <div class="col-8">
                                <input name="cidade_planta" type="text" style="outline:none" class="textCriarCliente" required>
                            </div>
                            <div class="col-4">
                                <select name="estado_planta" style="outline:none" id="tipoCad" required>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>

                        </div>

                        <div style="display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Cadastrar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEditPlanta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/edita_planta.php" method="POST">
                <input type="hidden" id="id_planta_edit" name="id_planta_edit">
                    <h2 style="text-align-last: center;">Editar Planta</h2>
                    <h2 style="text-align-last: center; color:#999999;margin-top: 0px;font-size: 20px;">Insira as informações</h2>

                    <div style="margin-top:20px">
                        <div class="form-row">
                            <div class="col-8">
                                <input name="cidade_planta_edit" id="cidade_planta_edit" type="text" style="outline:none" class="textCriarCliente" required>
                            </div>
                            <div class="col-4">
                                <select name="estado_planta_edit" id="estado_planta_edit" style="outline:none;border: none;border-radius: 20px;height: 35px;color: #686868;font-weight: bold;background: #C4C4C4;width: 100%;"  required>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>

                        </div>

                        <div style="display: flex;place-content: center;justify-content: space-between;">

                            <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                            <button type="submit" class="CadUsuario btn btn-user btn-block">
                                Editar
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    function edit_planta(id) {
        $.get("php/get_plantas.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $('#id_planta_edit').val(id);
            $('#cidade_planta_edit').val(json[0].cidade);
            $('#estado_planta_edit').val(json[1].estado);

            $('#modalEditPlanta').modal("show");
        });
    }

    var inputSearch = document.getElementById('pesquisaCliente');
    inputSearch.addEventListener('keyup', function(e) {
        var key = e.which || e.keyCode;
        if (key == 13) {
            $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + this.value, function(data) {
                $('#tbody').html(data);
            });
        }
    });

    function pesquisa_expansionista() {
        var pesquisa = $('#pesquisaCliente').val();
        $.get("views/subviews/clientes-pesquisa.php?pesquisa=" + pesquisa, function(data) {
            $('#tbody').html(data);
        });
    }
</script>