<div class="container-fluid">
  <div class="card" style="margin: 10px;">
    <div class="row background-todos">
      <div class="col">
        <div style="text-align: -webkit-center;">
          <h2>Qual opção deseja acessar?</h2>
          <p class="descOfPlan" style="font-weight: bold;">Analise os clientes Agro</p>
          <div class="card-clientes">
            <div class="button-clientes" onclick="page('clientes-cadastrados')">
              <div style="height:100%;padding: 85px 10px;">
                <i style="color:#F6D838; font-size: 50px;" class="fas fa-bookmark"></i>
                <h3 style="font-weight: bold;color:#F6D838;">Clientes Cadastrados</h3>
                <h5 style="color:#fff; font-size:12px">Analise os clientes cadastrados da AgroEmpreender.</h5>
              </div>

            </div>
            <div class="button-clientes" onclick="page('clientes-analise')">
              <div style="height:100%;padding: 85px 10px;">
                <i style="color:#F6D838; font-size: 50px;" class="fas fa-book-open"></i>
                <h3 style="font-weight: bold;color:#F6D838;">Solicitações de Cadastro</h3>
                <h5 style="color:#fff; font-size:12px">Confira as solicitações de cadastro.</h5>       
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

