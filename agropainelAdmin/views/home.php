<?php
include_once("../../conn/conexao.php");
session_start();

$id_user = $_SESSION['ZWxldHJpY2Ftadm'];

$sql = "SELECT * FROM user_admin WHERE id=$id_user";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $nome = $row['nome'];
  $avatar = $row['avatar'];
}

$sql = "SELECT * FROM user_cliente WHERE status = 0";
$res = mysqli_query($conn, $sql);
$count_users_analise = mysqli_num_rows($res);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">
            <div class="col-xl-7 col-lg-7">
                <h2>Calendário</h2>
                <!-- <div class="slideshow-container" style="max-width: 650px;">

                  <div class="mySlides ">
                    <img src="img/Rectangle 5.png" style="width:100%">
                  </div>
                  <div class="mySlides ">
                    <img src="img/Rectangle 5.png" style="width:100%">
                  </div> -->
                <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a> -->
                <!-- <div style="text-align: center;">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                  </div>
                </div> -->
                <div style="position: absolute;bottom: 0px;">
                    <h2>Em análise</h2>
                    <div id="accordion">
                        <div class="row" style="flex-wrap: nowrap;padding: 20px;">
                            <div class="cardPlano col" style="margin-right: 10px;">
                                <div class="row" style="justify-content: center;">
                                    <h4 class="titleOfPlan" style="font-size:40px;"><?= $count_users_analise ?></h4>
                                    <h4 class="titleOfPlan" style="text-align: start;align-self: center;margin-left: 2px;">Usuários em<br> análise</h4>
                                </div>

                                <p class="descOfPlan">Solicitações de cadastro a serem analisadas.</p>
                            </div>
                            <div class="cardPlano col" style="margin-right: 10px;">
                                <div class="row" style="justify-content: center;">
                                    <h4 class="titleOfPlan" style="font-size:40px;">03</h4>
                                    <h4 class="titleOfPlan" style="text-align: start;align-self: center;margin-left: 2px;">Créditos<br> em análise</h4>
                                </div>

                                <p class="descOfPlan">Documentos de solicitações de crédito a serem analisados.</p>
                            </div>
                            <div class="cardPlano col" style="margin-right: 10px;">
                                <div class="row" style="justify-content: center;">
                                    <h4 class="titleOfPlan" style="font-size:40px;">05</h4>
                                    <h4 class="titleOfPlan" style="text-align: start;align-self: center;margin-left: 2px;">Dúvidas<br> em espera</h4>
                                </div>

                                <p class="descOfPlan">Dúvidas a serem analisadas através do bate-papo.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div style="margin-left: 20px;" class="col">
                <div style="height: 100%;margin-bottom: 27px;">
                    <h2>Meu perfil</h2>
                    <div class="gray-div">
                        <div class="perfil-index">
                            <img class="rounded-circle" src="data:image/png;base64,<?= $avatar ?>" style="width:150px; height:150px;" alt="">
                            <div style="    align-self: center; padding-left:20px ">
                                <h4 class="titleOfPlan" style=" font-size:20px"><?= $nome ?></h4>
                                <p class="descOfPlan" style="font-weight: bold;">Ache o consórcio ideal para você. Planos odontológicos, estéticos, automóveis e etc.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>