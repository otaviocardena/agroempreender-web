<form action="php/cadastra_usuarios.php" method="POST" id="form-cadastra-user" enctype="multipart/form-data">
    <div style="display:block;">
        <div class="form-row" style="width: 100%;">
            <div class="col-lg-3 col-xs-12 mb-2" style="text-align: -webkit-center;align-self: center;">
                <input onchange="mostrarimg(this)" id="img_user" type="file" name="img_user" style="display:none;" />
                <img onclick="abrir_input_img()" id="falseinput" class="rounded-circle" src="img/Perfilteste.png" style="cursor:pointer;width:130px;height:130px" alt="">
            </div>
            <div class="col">
                <div class="form-row" style="width: 100%;">
                    <div class="col">
                        <h5 class="name-users">Login</h5>
                        <input name="login_user" id="login_user" style="width:100%;border: none;outline: none;border-radius: 15px;" type="text">
                    </div>
                    <div class="col">
                        <h5 class="name-users">Senha</h5>
                        <input name="senha_user" id="senha_user" style="width:100%;border: none;outline: none;border-radius: 15px;" type="password">
                    </div>

                </div>
                <div class="form-row" style="width: 100%;">
                    <div class="col">
                        <h5 class="name-users">E-mail de contato</h5>
                        <input name="email_user" id="email_user" style="border: none;outline: none;border-radius: 15px; width:100%" type="text">
                    </div>

                </div>
                <div class="form-row" style="width: 100%;">
                    <div class="col">
                        <h5 class="name-users">Nome do usuário</h5>
                        <input name="nome_user" id="nome_user" style="border: none;outline: none;border-radius: 15px; width:100%" type="text">
                    </div>

                </div>
            </div>
        </div>
        <div class="form-row" style="width: 100%;margin-top: 20px;text-align-last: center;">
            <div class="col">
                <h5 style="font-size: 25px" class="name-users">Permissões</h5>
            </div>
        </div>
        <div class="form-row" style="position:relative;width: 100%;margin-top:20px">
            <div class="form-row" style="width: 100%;text-align-last: center;">
                <div class="col">
                    <input onclick="altera_icone(1)" id="toggle1" name="toggle1" type="checkbox">

                    <label class="checkUser" for="toggle1">
                        <i id="icon1" style="margin-right:4px" class="fas fa-plus"></i>
                        Clientes
                    </label>
                </div>
                <div class="col">
                    <input onclick="altera_icone(2)" id="toggle2" name="toggle2" type="checkbox">
                    <label class="checkUser" for="toggle2">
                        <i id="icon2" style="margin-right:4px" class="fas fa-plus"></i>
                        Documentações
                    </label>
                </div>
                <div class="col">
                    <input onclick="altera_icone(3)" id="toggle3" name="toggle3" type="checkbox">

                    <label class="checkUser" for="toggle3">
                        <i id="icon3" style="margin-right:4px" class="fas fa-plus"></i>
                        Relatórios
                    </label>
                </div>
            </div>
            <div class="form-row" style="width: 100%;text-align-last: center;">
                <div class="col" style="padding-left:100px">
                    <input onclick="altera_icone(4)" id="toggle4" name="toggle4" type="checkbox">

                    <label class="checkUser" for="toggle4">
                        <i id="icon4" style="margin-right:4px" class="fas fa-plus"></i>
                        Serviços
                    </label>
                </div>
                <div class="col" style="padding-right:100px"><input onclick="altera_icone(5)" id="toggle5" name="toggle5" type="checkbox">s 

                    <label class="checkUser" for="toggle5">
                        <i id="icon5" style="margin-right:4px" class="fas fa-plus"></i>
                        Usuários
                    </label>
                </div>
            </div>
        </div>
        <div class="form-row" style="width: 100%;margin-top: 35px;text-align-last: center;">
            <div class="col">
                <h5 style="font-size: 25px" class="name-users">Informações Adicionais</h5>
            </div>
        </div>
        <div class="form-row" style="margin-top:10px">
            <div class="col-4"></div>

            <div class="col">
                <div class="form-row">
                    <h5 class="name-users">CPF</h5>
                </div>
                <div class="form-row">
                    <input name="cpf_user" id="cpf_user" style="width:100%;border: none;outline: none;border-radius: 15px;" type="text">
                </div>
                <div class="form-row">
                    <h5 class="name-users">Celular</h5>
                </div>
                <div class="form-row">
                    <input name="celular_user" id="celular_user" style="width:100%;border: none;outline: none;border-radius: 15px;" type="text">
                </div>
            </div>
            <div class="col-4"></div>

        </div>
        <div class="form-row" style="    place-content: center;">
            <button type="submit" class="CadUsuario btn btn-user btn-block">
                Confirmar
            </button>
            <button type="button" onclick="cancela_alteracoes()" style="margin-left:10px" class="CancelaUsuario btn btn-user btn-block">
                Cancelar
            </button>
        </div>
    </div>
</form>

<script>
    function abrir_input_img() {
        $("#img_user").click();
    }

    function mostrarimg(obj) {
        var file = document.getElementById(obj.id).files;

        if (file.length > 0) {
            var reader = new FileReader();

            reader.onload = function(e) {
                document.getElementById('falseinput').setAttribute("src", e.target.result);
            }

            reader.readAsDataURL(file[0]);
        }
    }
</script>