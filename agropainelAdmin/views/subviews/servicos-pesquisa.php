<?php
include_once("../../../conn/conexao.php");
$pesquisa = $_GET['pesquisa'];

$sql = "SELECT 
            cs.id AS id_cliente_servico,
            cs.id_servico AS id_servico,
            cs.id_cliente AS id_cliente_aberto,
            uc.nome AS nome_cliente,
            uc.avatar AS foto_cliente,
            s.nome AS nome_servico,
            cs.data_cad,
            cse.etapa
        FROM cliente_servico AS cs 
        INNER JOIN cliente_servico_etapa AS cse
            ON cs.id = cse.id_cliente_servico
        INNER JOIN servicos AS s 
            ON cs.id_servico = s.id
        INNER JOIN user_cliente AS uc
            ON cs.id_cliente = uc.id
        WHERE 
            (cse.status = 1 OR cse.status = 0) AND
            (s.nome LIKE '%$pesquisa%' OR uc.nome LIKE '%$pesquisa%')
        GROUP BY 
            cse.id_cliente_servico";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
?>
    <tr>
        <td>
            <img src="data:image/png;base64,<?= $row['foto_cliente'] ?>" style="width:50px;height:50px;border-radius:100%" alt="">
            <?= $row['nome_cliente'] ?>
        </td>
        <td><?= date('d/m/Y', strtotime($row['data_cad'])) ?></td>
        <td><?= $row['nome_servico'] ?></td>
        <td><?= "Etapa " . $row['etapa'] ?></td>
        <td>
            <button style="outline:none" class="icon-plusClientes" onclick="abrir_plano_info(<?= $row['id_cliente_aberto'] ?>,<?= $row['id_cliente_servico'] ?>)">
                <i class="fas fa-plus"></i>
            </button>
        </td>
    </tr>
<?php } ?>