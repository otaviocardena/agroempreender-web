<?php
session_start();
include_once("../../../conn/conexao.php");
$id_cliente_aberto = $_GET['id_cliente_aberto'];

$sql = "SELECT * FROM user_cliente WHERE id = $id_cliente_aberto";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $cpf = $row['cpf'];
    $email = $row['email'];
    $telefone = $row['telefone'] ? $row['telefone'] : "Não há informação";
    $celular = $row['celular'] ? $row['celular'] : "Não há informação";
}

$sql = "SELECT * FROM enderecos_cliente WHERE id_cliente = $id_cliente_aberto";
$res = mysqli_query($conn, $sql);
?>
<div class="col">
    <div class="form-row">
        <h2>Informações Pessoais</h2>
    </div>
    <div id="accordion1" style="height:70vh;margin: 20px 0px;overflow-x:hidden;">
        <div>
            <p class="info-ClientesText">CPF: <span style="color:#686868"><?= $cpf ?></span></p>
            <p class="info-ClientesText">E-mail: <span style="color:#686868"><?= $email ?></span></p>
            <p class="info-ClientesText">Telefone: <span style="color:#686868"><?= $telefone ?></span></p>
            <p class="info-ClientesText">Celular: <span style="color:#686868"><?= $celular ?></span></p>
        </div>
        <div class="form-row">
            <h2 style="margin-left: 2px;">Endereços</h2>
        </div>
        <div>
            <?php foreach ($res as $key => $value) { ?>
                <button class="buttonEndereco" style="outline:none; display:flex;margin-top:10px; width:100%">
                    <div style="border-radius: 100%;place-self: center;font-size: 35px;background: #21613A;padding: 15px 20px; margin-right:10px">
                        <i style="color:#F6D838" class="fas fa-home"></i>
                    </div>
                    <div style="display:block;text-align: start;margin-right: 15px;">
                        <p class="info-EnderecoCliente">Endereço <?= $key + 1 ?></p>
                        <p class="info-EnderecoCliente1"><?= $value['endereco'] . ", " . $value['numero'] . ", " . $value['bairro'] ?></p>
                        <p class="info-EnderecoCliente1"><?= $value['cidade'] . ", " . $value['estado'] ?></p>
                        <p class="info-EnderecoCliente1"><?= $value['cep'] ?></p>
                    </div>
                </button>
            <?php } ?>
        </div>
    </div>

</div>