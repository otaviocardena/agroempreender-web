<form action="php/cadastra_consorcios.php" method="POST" enctype="multipart/form-data">
    <div class="form-row" style="justify-content: center;margin-top: 50px;">
        <div style="display:flex;margin-left: 20px;">
            <input type="text" id="tituloConsorcio" name="titulo_consorcio" placeholder="Título">
        </div>
    </div>
    <div class="form-row" style="justify-content: center;">
        <div style="display:flex;margin-left: 20px;margin-bottom:10px">
        <textarea  style="resize:none;text-align-last: start; width:100%" rows="3" type="text" id="subtituloConsorcio" name="subtitulo_consorcio" placeholder="Insira aqui o subtítulo"></textarea>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;">
        <div style="display:block;text-align: center;">
            <img id="icone-show" src="./img/archive.png" style="height: 76px;">
            <input onchange="mostrarimg(this,1)" type="file" id="icone_consorcio" name="icone_consorcio" accept=".png,.ico,.svg" style="display:none">
            <div id="buttonCriarNovo1" class="btn btn-user btn-block" onclick="abrir_input(1)">
                Alterar Ícone
            </div>
        </div>
        <div style="display:block;text-align: center;margin-left:10px">
            <img id="img-show" src="./img/images.png" style="height: 76px;">
            <input onchange="mostrarimg(this,2)" type="file" id="img_consorcio" name="img_consorcio" style="display:none" accept=".jpg,.jpeg,.png,.bmp">
            <div id="buttonCriarNovo1" class="btn btn-user btn-block" onclick="abrir_input(2)">
                Alterar Imagem
            </div>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <div style="display:flex;margin-left: 20px;">
            <textarea class="textArea" maxlength="500" id="textAreaConsorcio" name="descricao_consorcio" rows="4" cols="50"></textarea>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <input onclick="altera_icone(4)" id="toggle4" name="status_consorcio" type="checkbox">

        <label style="width: 30%;" class="checkUser" for="toggle4">
            <i id="icon4" style="margin-right:4px" class="fas fa-plus"></i>
            <span id="txt_status">Ativo</span>
        </label>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <button type="submit" id="buttonCriarNovo1" class="btn btn-user btn-block">
            Confirmar
        </button>
    </div>
</form>
