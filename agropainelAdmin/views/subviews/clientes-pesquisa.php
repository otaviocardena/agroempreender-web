<?php 
    include_once("../../../conn/conexao.php");
    $pesquisa = $_GET['pesquisa'];

    $sql = "SELECT * FROM user_cliente WHERE nome LIKE '%$pesquisa%' AND status = 1";
    $res = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_array($res)) {
    if ($row['status'] == 1) {
        $status = "Ativo";
    } else if ($row['status'] == 2) {
        $status = "Desativado";
    }
?>
    <tr>
        <td>
            <img src="data:image/png;base64,<?= $row['avatar'] ?>" style="width:50px;height:50px;border-radius:100%" alt="">
            <?= $row['nome'] ?>
        </td>
        <td><?= date('d/m/Y', strtotime($row['data_aprovacao'])) ?></td>
        <td>CADESP</td>
        <td><?= $status ?></td>
        <td>
            <button style="outline:none" class="icon-plusClientes" onclick="abrir_cliente(<?= $row['id'] ?>)">
                <i class="fas fa-plus"></i>
            </button>

        </td>
    </tr>
<?php } ?>