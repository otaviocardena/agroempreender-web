<?php
    include_once("../../../conn/conexao.php");
    $id_cliente_aberto = $_GET['id_cliente_aberto'];
    $sql = "SELECT
                td.nome,
                dc.data_cad,
                dc.id as id_doc
            FROM documentos_cliente AS dc
            INNER JOIN tipo_documento AS td ON
                dc.tipo_doc = td.id
            WHERE 
                dc.id_cliente = $id_cliente_aberto AND
                td.tipo = 1 AND
                dc.status = 2
                ";
    $res_doc_cli = mysqli_query($conn,$sql);
?>
<div style="display:flex">
    <h2>Documentação</h2>
    <div style="right: 40px;top: 36px;position: absolute;">
        <input id="pesquisaDocumento" type="text" placeholder="Pesquisar documento...">
        <i class="fas fa-search botao-pesquisar" onclick=""></i>
        </input>
    </div>
</div>
<div>
    <div id="accordion1" style="height:70vh;margin: 20px 0px;">
        <table id="example" class="table" style="width:100%;margin-top:20px">
            <thead>
                <tr>
                    <th style="border-bottom: none; color:#21613A">Nome Documento</th>
                    <th style="border-bottom: none;color:#21613A">Publicado em</th>
                    <th style="border-bottom: none;color:#21613A"></th>
                </tr>
            </thead>
            <tbody id="tbody">
                <?php while ($row = mysqli_fetch_array($res_doc_cli)) { ?>
                    <tr>
                        <td>
                            <a><?= $row['nome'] ?>.pdf</a>
                        </td>
                        <td><?= date('d/m/Y',strtotime($row['data_cad'])) ?></td>
                        <td>
                            <a target="_blank" href="php/download_arquivos.php?id=<?= $row['id_doc'] ?>&tabela=cliente" style="outline:none; margin:2%;" class="icon-plusClientes">
                                <i style="color: #F6D838" class="fas fa-cloud-download-alt"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>