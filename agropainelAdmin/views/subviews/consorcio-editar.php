<?php
include_once("../../../conn/conexao.php");
$id_consorcio_aberto = $_GET['id_consorcio_aberto'];

$sql = "SELECT * FROM consorcios WHERE id = $id_consorcio_aberto";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $titulo = $row['titulo'];
    $subtitulo = $row['subtitulo'];
    $icone = $row['icone'];
    $imagem = $row['imagem'];
    $descricao = $row['descricao'];
    $status_valor = $row['status'];
    $status = $row['status'] == 1 ? "Inativo" : "Ativo";
}
?>
<form action="php/edita_consorcios.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="id_consorcio" value="<?= $id_consorcio_aberto ?>">
    <div class="form-row" style="justify-content: center;margin-top: 50px;">
        <div style="display:flex;margin-left: 20px;">
            <input type="text" id="tituloConsorcio" name="titulo_consorcio" placeholder="Título" value="<?= $titulo ?>">
        </div>
    </div>
    <div class="form-row" style="justify-content: center;">
        <div style="display:flex;margin-left: 20px;margin-bottom:10px">
            <textarea style="resize:none;text-align-last: start; width:100%" rows="3" type="text" id="subtituloConsorcio" name="subtitulo_consorcio" placeholder="Insira aqui o subtítulo"><?= $subtitulo ?></textarea>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;">
        <div style="display:block;text-align: center;">
            <img id="icone-show" src="data:image/png;base64,<?= $icone ?>" style="height: 76px;">
            <input onchange="mostrarimg(this,1)" type="file" id="icone_consorcio" name="icone_consorcio" accept=".png,.ico,.svg" style="display:none">
            <div id="buttonCriarNovo1" class="btn btn-user btn-block" onclick="abrir_input(1)">
                Alterar Ícone
            </div>
        </div>
        <div style="display:block;text-align: center;margin-left:10px">
            <img id="img-show" src="data:image/png;base64,<?= $imagem ?>" style="height: 76px;">
            <input onchange="mostrarimg(this,2)" type="file" id="img_consorcio" name="img_consorcio" style="display:none" accept=".jpg,.jpeg,.png,.bmp">
            <div id="buttonCriarNovo1" class="btn btn-user btn-block" onclick="abrir_input(2)">
                Alterar Imagem
            </div>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <div style="display:flex;margin-left: 20px;">
            <textarea class="textArea" style="resize:none;" maxlength="500" id="textAreaConsorcio" name="descricao_consorcio" rows="4" cols="50"><?= $descricao ?></textarea>
        </div>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <input onclick="altera_icone(4)" id="toggle4" name="status_consorcio" type="checkbox">

        <label style="width: 30%;" class="checkUser" for="toggle4">
            <i id="icon4" style="margin-right:4px" class="fas fa-plus"></i>
            <span id="txt_status"><?= $status ?></span>
        </label>
    </div>
    <div class="form-row" style="justify-content: center;margin-top:20px">
        <button type="submit" id="buttonCriarNovo1" class="btn btn-user btn-block">
            Confirmar
        </button>
    </div>
</form>

<script>
    $(document).ready(function() {
        if (<?= $status_valor ?> == 1) {
            $('#toggle4').attr('checked', 'checked');
            $('#icon4').removeClass('fa-plus').addClass('fa-minus');
        } else {
            $('#icon4').removeClass('fa-minus').addClass('fa-plus');
        }
    });
</script>