<?php
include_once("../../../conn/conexao.php");
$id_cliente_aberto = $_GET['id_cliente_aberto'];
$sql = "SELECT 
            cs.id as id_cliente_servico,
            cs.id_servico AS id_servico,
            cs.herdeiro_1, 
            cs.herdeiro_2,
            s.nome AS nome_servico,
            cs.data_cad,
            cse.etapa,
            cse.status
        FROM cliente_servico AS cs
        INNER JOIN cliente_servico_etapa as cse
            ON cs.id = cse.id_cliente_servico
        INNER JOIN servicos AS s 
            ON cs.id_servico = s.id 
        WHERE (cse.status = 1 OR cse.status = 0) 
            AND cs.id_cliente=$id_cliente_aberto 
        GROUP BY 
            cse.id_cliente_servico";
$res = mysqli_query($conn, $sql);

function getImagemHerdeiro($id)
{
    global $conn;
    $sql = "SELECT avatar FROM user_cliente WHERE id = $id";
    $res = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($res)) {
        $avatar = $row[0];
    }
    return $avatar;
}

?>
<input type="hidden" name="id_cliente_plano" id="id_cliente_plano" value="<?= $id_cliente_aberto ?>">
<div class="col">
    <div class="form-row">
        <h2>Plano do Cliente</h2>
    </div>
    <div id="accordion1" style="height:70vh;margin: 20px 0px;">

        <!-- plano -->
        <?php while ($row = mysqli_fetch_array($res)) {
            if ($row['status'] == 0) {
                $status = "Fechado";
            } else if ($row['status'] == 1) {
                $status = "Em andamento";
            } else if ($row['status'] == 2) {
                $status = "Aprovado";
            } ?>
            <div class="form-row planos-butHover" style="background: #efefef;border-radius: 20px;margin-right: 5px;margin-left: 5px;margin-top:5px;cursor: pointer;" onclick="abrir_plano_info(<?= $id_cliente_aberto ?>,<?=$row['id_cliente_servico'] ?>)">
                <div style="background: #21613A;border-radius:20px;">
                    <img src="img/file.png" alt="" style="padding: 29px;">
                </div>


                <div style="text-align: start;padding: 10px;">
                    <div style="display:flex;">
                        <h4 class="servicosButton-Green"><?= $row['nome_servico'] ?></h4>
                        <?php
                        if ($row['herdeiro_1']) { ?>
                            <img style="margin-left:5px;width:26px; height:26px;" class="rounded-circle" src="data:image/png;base64,<?= getImagemHerdeiro($row['herdeiro_1']) ?>">
                        <?php }
                        if ($row['herdeiro_2']) { ?>
                            <img style="margin-left:5px;width:26px; height:26px;" class="rounded-circle" src="data:image/png;base64,<?= getImagemHerdeiro($row['herdeiro_2']) ?>">
                        <?php } ?>
                    </div>
                    <h4 class="servicosButton-Grey">Descrição do processo.</h4>
                    <h4 class="servicosButton-Grey">Contratado em: <span class="span-plano" style="color:#21613A"><?= date('d/m/Y', strtotime($row['data_cad'])) ?></span></h4>
                    <h4 class="servicosButton-Grey">Status: <span class="span-plano" style="color:#21613A"><?= $status ?> - Etapa <?= $row['etapa'] ?></span></h4>
                </div>


            </div>
        <?php } ?>
        <!-- plano -->
    </div>


</div>

<script>
    function abrir_plano_info(id_cli, id_cli_serv) {
        var id = $('#id_cliente_plano').val();
        var data = "<div id='spinner' class='spinner-border' role='status' style='height:5rem;width:5rem;margin-left: 44%;margin-top: 25%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
        $("#conteudo").html(data);
        $.get("views/plano-cliente-info.php?id_cliente_aberto=" + id_cli + "&id_cliente_servico=" + id_cli_serv, function(data) {
            $('#conteudo').html(data);
        });
    }
</script>