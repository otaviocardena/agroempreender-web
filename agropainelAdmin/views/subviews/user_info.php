<?php
include_once("../../../conn/conexao.php");
session_start();

$id_user_aberto = $_GET['id_user_aberto'];

$sql = "SELECT permission FROM user_permission WHERE id_user = $id_user_aberto";
$res_permission_user = mysqli_query($conn, $sql);

$permissions_user_aberto = array();

while ($row = mysqli_fetch_array($res_permission_user)) {
    array_push($permissions_user_aberto, $row['permission']);
}

$sql = "SELECT * FROM user_admin WHERE id=$id_user_aberto";
$res_user_aberto = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res_user_aberto)) {
    $avatar = $row['avatar'];
    $login = $row['usuario'];
    $senha = $row['senha'];
    $email = $row['email'];
    $nome = $row['nome'];
    $cpf = $row['cpf'];
    $celular = $row['celular'];
}
?>


<form action="php/edita_usuarios.php" method="POST" id="form-edita-user" enctype="multipart/form-data">
    <input type="hidden" value="<?= $id_user_aberto ?>" name="id_user_aberto" id="id_user_aberto">
    <input onchange="mostrarimg(this)" id="img_user" type="file" name="img_user" style="display:none;" />
    <div style="display:block;">
        <div class="form-row" style="width: 100%;">
            <div class="col-lg-3 col-xs-12 mb-2" style="text-align: -webkit-center;align-self: center;">
                <img onclick="abrir_input_img()" id="falseinput" class="rounded-circle" src="data:image/png;base64,<?= $avatar ?>" style="cursor:pointer;width:130px;height:130px" alt="">
            </div>
            <div class="col">
                <div class="form-row" style="width: 100%;">
                    <div class="col">
                        <h5 class="name-users">Login</h5>
                        <input name="login_user" id="login_user" value="<?= $login ?>" class="camposinformacoes" placeholder="" type="text"></input>


                    </div>
                    <div class="col">
                        <h5 class="name-users">Senha</h5>
                        <input name="senha_user" id="senha_user" value="<?= $senha ?>" class="camposinformacoes" placeholder="" type="password"></input>
                    </div>

                </div>
                <div class="form-row" style="width: 100%;">
                    <div class="col">
                        <h5 class="name-users">E-mail de contato</h5>
                        <input name="email_user" id="email_user" value="<?= $email ?>" class="camposinformacoes" placeholder="" type="text"></input>
                    </div>

                </div>
                <div class="form-row" style="    width: 100%;">
                    <div class="col">
                        <h5 class="name-users">Nome do usuário</h5>
                        <input name="nome_user" id="nome_user" value="<?= $nome ?>" class="camposinformacoes" placeholder="" type="text"></input>
                    </div>
                </div>
            </div>
        </div>



        <div class="form-row" style="width: 100%;margin-top: 20px;text-align-last: center;">
            <div class="col">
                <h5 style="font-size: 25px" class="name-users">Permissões</h5>
            </div>
        </div>
        <div class="form-row" style="position:relative;width: 100%;margin-top:20px">
            <div class="form-row" style="width: 100%;text-align-last: center;">
                <div class="col">
                    <input onclick="altera_icone(1)" id="toggle1" name="toggle1" type="checkbox">

                    <label class="checkUser" for="toggle1">
                        <i id="icon1" style="margin-right:4px" class="fas fa-plus"></i>
                        Clientes
                    </label>
                </div>
                <div class="col">
                    <input onclick="altera_icone(2)" id="toggle2" name="toggle2" type="checkbox">
                    <label class="checkUser" for="toggle2">
                        <i id="icon2" style="margin-right:4px" class="fas fa-plus"></i>
                        Documentações
                    </label>
                </div>
                <div class="col">
                    <input onclick="altera_icone(3)" id="toggle3" name="toggle3" type="checkbox">

                    <label class="checkUser" for="toggle3">
                        <i id="icon3" style="margin-right:4px" class="fas fa-plus"></i>
                        Relatórios
                    </label>
                </div>
            </div>
            <div class="form-row" style="width: 100%;text-align-last: center;">
                <div class="col" style="padding-left:100px">
                    <input onclick="altera_icone(4)" id="toggle4" name="toggle4" type="checkbox">

                    <label class="checkUser" for="toggle4">
                        <i id="icon4" style="margin-right:4px" class="fas fa-plus"></i>
                        Serviços
                    </label>
                </div>
                <div class="col" style="padding-right:100px"><input onclick="altera_icone(5)" id="toggle5" name="toggle5" type="checkbox">

                    <label class="checkUser" for="toggle5">
                        <i id="icon5" style="margin-right:4px" class="fas fa-plus"></i>
                        Usuários
                    </label>
                </div>
            </div>
        </div>
        <div class="form-row" style="width: 100%;margin-top: 35px;text-align-last: center;">
            <div class="col">
                <h5 style="font-size: 25px" class="name-users">Informações Adicionais</h5>
            </div>
        </div>
        <div class="form-row" style="margin-top:10px">
        <div class="col-1"></div>
            <div class="col-5">
            <div class="form-row">
                    <h5 class="name-users">CPF</h5>
                </div>
                <div class="form-row">
                    <input name="cpf_user" id="cpf_user" value="<?= $cpf ?>" class="campocpf" placeholder="" type="text"></input>
                </div>
            </div>
            
            <div class="col-5">
                
                <div class="form-row">
                    <h5 class="name-users">Celular</h5>
                </div>
                <div class="form-row">
                    <input name="celular_user" id="celular_user" value="<?= $celular ?>" class="campocelular" placeholder="" type="text"></input>
                </div>
            </div>
            <div class="col-1"></div>

        </div>
        <div class="form-row" style="    place-content: center;">
            <button type="submit" class="CadUsuario btn btn-user btn-block">
                Confirmar
            </button>
            <button type="button" onclick="cancela_alteracoes()" style="margin-left:10px" class="CancelaUsuario btn btn-user btn-block">
                Cancelar
            </button>
        </div>
    </div>
</form>

<!-- PARA PREENCHER OS CHECK DE PERMISSÕES -->
<script>
    <?php
    foreach ($permissions_user_aberto as $key => $value) {
        if (in_array($value, $permissions_user_aberto)) {
    ?>
            $('#toggle' + <?= $value ?>).attr('checked', 'checked');
            if (document.getElementById("toggle" + <?= $value ?>).checked == true) {
                $('#icon' + <?= $value ?>).removeClass('fa-plus').addClass('fa-minus');
            } else {
                $('#icon' + <?= $value ?>).removeClass('fa-minus').addClass('fa-plus');
            }
    <?php
        }
    }
    ?>

    function abrir_input_img() {
        $("#img_user").click();
    }

    function mostrarimg(obj) {
        var file = document.getElementById(obj.id).files;

        if (file.length > 0) {
            var reader = new FileReader();

            reader.onload = function(e) {
                document.getElementById('falseinput').setAttribute("src", e.target.result);
            }

            reader.readAsDataURL(file[0]);
        }
    }
</script>