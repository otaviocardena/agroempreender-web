<div class="container-fluid">
  <div class="card" style="margin: 10px;">
    <div class="row background-todos">
      <div class="col">
        <div style="text-align: -webkit-center;">
          <h2>Qual opção deseja acessar?</h2>
          <p class="descOfPlan" style="font-weight: bold;">Consulte ou edite os processos dos seus serviços.</p>
          <div class="card-clientes">
            <div class="button-clientes" onclick="page('servicos-clientes')">
              <div style="height:100%;padding: 85px 10px;">
                <img src="./img/consorcio.png" alt="">
                <h3 style="font-weight: bold;color:#F6D838;">Servicos em Andamento</h3>
                <h5 style="color:#fff; font-size:12px">Consulte os serviços que estão em processo na Agroempreender.</h5>
              </div>

            </div>
            <div class="button-clientes" onclick="page('servicos')">
              <div style="height:100%;padding: 85px 10px;">
                <i style="color:#F6D838; font-size: 50px;" class="fas fa-book-open"></i>
                <h3 style="font-weight: bold;color:#F6D838;">Painel de Serviços</h3>
                <h5 style="color:#fff; font-size:12px">Consulte os seus serviços.</h5>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>