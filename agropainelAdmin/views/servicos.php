<?php
include_once("../../conn/conexao.php");

$sql = "SELECT * FROM servicos";
$res = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
  <div class="card" style="margin: 10px;">
    <div class="row" style="margin: 10px 20px 20px 20px;">
      <div style="display:flex;margin-left: 20px;">
        <h2>Serviços</h2>
      </div>
      <div id="accordion1" style="height:70vh;margin: 20px 10px;">
        <div style="display: grid;grid-template-columns: 20% 20% 20% 20% 20%;place-items: center;">
          <?php while ($row = mysqli_fetch_array($res)) { ?>
            <div class=" button-telaServicos col-10" <?php if ($row['id'] == 14) { ?> onclick="page('consorcios')" <?php } ?>>
              <div style="height:100%;padding: 50px 10px;text-align: -webkit-center;">
                <img src="data:image/png;base64,<?= $row['icone'] ?>" style="width:50px" alt="">
                <h3 style="font-weight: bold;color:#F6D838;"><?= $row['nome'] ?></h3>
                <!-- <h5 style="color:#fff; font-size:12px">A opção ideal para seus planos.</h5> -->
                <?php if ($row['status'] == 1) { ?>
                  <button id="buttonDesativar" class="btn buttonInfoCliente" style="width:auto;outline: none;display: flex;padding: 5px;justify-content: center;align-items: center;outline: none;" onclick="alterar_status_servico(<?= $row['id'] ?>,0)">
                    <i style="font-size:20px" class="fas fa-power-off"></i>
                    <p style="margin-left:5px;margin-bottom: 0px;">Desativar</p>
                  </button>
                <?php } else if ($row['status'] == 0) { ?>
                  <button class="btn buttonInfoCliente" style="width:auto;outline: none;display: flex;padding: 5px;justify-content: center;align-items: center;outline: none;" onclick="alterar_status_servico(<?= $row['id'] ?>,1)">
                    <i style="font-size:25px" class="fas fa-power-off"></i>
                    <p style="margin-left:5px;margin-bottom: 0px;">Ativar</p>
                  </button>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </div>

      </div>


    </div>
  </div>

</div>

<script>
  function alterar_status_servico(id, status) {
    $.get("php/alterar_visibilidade_servico.php?id=" + id + "&status=" + status, function(data) {
      if (data != "Fail") {
        window.location.href = data;
        location.reload();
      }
    });
  }
</script>