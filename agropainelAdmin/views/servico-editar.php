<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row" style="margin: 10px 20px 20px 20px;">
            <div style="display:flex;width: 100%;">
                <h2>Credito</h2>
                <button id="buttonCriarNovo1" class="btn btn-user btn-block" style="margin-left:30px"  onclick="">
                    <i class="fas fa-undo"></i>
                    Voltar
                </button>
            </div>
            <div class="form-row" style="width:100%">
                <div class="col-xl-5 col-lg-5" >
                    <div id="accordion1" style="height:70vh;margin: 20px 0px;">
                        <button class="buttonServico" style="display:flex; margin-top:10px">
                            <div class="insideButton">
                            <div style=" width: 108px;   margin-top: 10px;">
                                <img src="img/document.png" alt="">
                            </div>
                            
                            </div> 
                            <div style="text-align: start;padding: 10px;">
                            <h4 class="servicosButton-Green">Etapa 01</h4>
                            <h4 class="servicosButton-Grey">Documentação Proprietária.</h4>
                            <h4 class="servicosButton-Grey">Status: <span style="color:#feb548">Em Andamento</span></h4>
                            </div>
                        </button>
                        <button class="buttonServico" style="display:flex; margin-top:10px">
                            <div class="insideButton">
                            <div style=" width: 108px;   margin-top: 10px;">
                                <img src="img/document.png" alt="">
                            </div>
                            
                            </div> 
                            <div style="text-align: start;padding: 10px;">
                            <h4 class="servicosButton-Green">Etapa 01.1</h4>
                            <h4 class="servicosButton-Grey">Documentação Proprietária.</h4>
                            <h4 class="servicosButton-Grey">Status: <span style="color:#feb548">Em Andamento</span></h4>
                            </div>
                        </button>
                    </div>
                </div>
                <div class="col-6" style="background:#EDEDED; border-radius:25px;    padding-right: 20px;">
                    <div class="form-row" style="justify-content: center;margin-top: 50px;">
                        <div style="display:flex;margin-left: 20px;">
                            <input type="text" id="tituloConsorcio" placeholder="Título">
                            
                        </div>
                    </div>
                    <div class="form-row" style="justify-content: center;">
                        <div style="display:flex;margin-left: 20px;">
                            <input type="text" id="subtituloConsorcio" placeholder="Insira aqui o sub título">
                        </div>
                    </div>
                    <div class="form-row" style="justify-content: center;">
                        <div style="display:block;text-align: center;">
                            <img src="./img/archive.png" alt="">
                            <button id="buttonCriarNovo1" class="btn btn-user btn-block"  onclick="">
                                Alterar Ícone
                            </button>
                        </div>
                    </div>
                    <div class="form-row" style="justify-content: center;margin-top:20px">
                        <div style="display:flex;margin-left: 20px;">
                            <textarea style="text-align: start;resize:none" id="textAreaConsorcio" rows="4" cols="50">
                            </textarea>
                        </div>
                    </div>
                    <div class="form-row" style="justify-content: center;margin-top:20px">
                            <button id="buttonCriarNovo1" class="btn btn-user btn-block"  onclick="">
                                Confirmar
                            </button>
                            <button id="buttonCriarNovo2" style="margin-left:20px" class="btn btn-user btn-block"  onclick="">
                                Deletar
                            </button>
                    </div>
                    
                    
                </div>
            </div>
            
        </div>
    </div>
</div>