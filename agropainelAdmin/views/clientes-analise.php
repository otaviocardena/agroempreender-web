<?php
include_once("../../conn/conexao.php");
$sql = "SELECT * FROM user_cliente WHERE status = 0";
$res = mysqli_query($conn, $sql);
?>
<div class="container-fluid">
    <div class="card" style="margin: 10px;">
        <div class="row background-todos">
            <div style="display:flex">
                <h2>Clientes em análise</h2>
                <div style="right: 40px;top: 36px;position: absolute;">
                    <input id="pesquisaCliente" type="text" placeholder="Pesquisar cliente...">
                    <i class="fas fa-search botao-pesquisar" onclick="pesquisa_cliente()"></i>
                    </input>
                </div>
            </div>
            <div id="accordion1" style="height:70vh;margin: 20px 0px;overflow-x:hidden;" class="row">
                <!--<div id="div-clientes-aprov" style="display: grid;grid-template-columns: 50% 50%;"> -->
                <div id="div-clientes-aprov" class="col-lg-6 col-sm-12">
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <a class="users" onclick="abrir_modal_aprovacao(<?= $row['id'] ?>)" style="margin-left:5px; margin-right:5px; margin-bottom: 5px;">
                            <img class="rounded-circle" style="height:80px;width:80px" src="img/Perfilteste.png" alt="">
                            <div style=" margin-left:10px">
                                <h5 class="name-users"><?= $row['nome'] ?></h5>
                                <h5 class="info-users"><?= $row['email'] ?></h5>
                                <h5 class="info-users"><?= $row['celular'] ?></h5>
                                <h5 class="info-users"><?= $row['cpf'] ?></h5>
                            </div>
                        </a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalAprovacaoCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-body" style="text-align: -webkit-center;">
                <form action="php/aprova_cliente.php" method="POST">
                    <input type="hidden" name="id_cliente_aprov" id="id_cliente_aprov">
                    <div style="display:flex;    justify-content: center;">
                        <i style="color: #21613A;font-size: 55px;align-self: center;" class="fas fa-user-circle"></i>
                        <div style=" margin-left:10px;text-align: start;">
                            <h5 class="name-users" id="nome_cliente_aprov"></h5>
                            <h5 class="info-users" id="email_cliente_aprov"></h5>
                            <h5 class="info-users" id="celular_cliente_aprov"></h5>
                            <h5 class="info-users" id="cpf_cliente_aprov"></h5>
                        </div>
                    </div>

                    <div style="text-align: -webkit-center;"></div>
                    <div style="white-space: pre-line;">
                        <input name="login_cliente_aprov" type="text" style="outline:none" class="textCriarCliente" placeholder="Login" required>
                        <input name="senha_cliente_aprov" type="password" style="outline:none" class="textCriarCliente" placeholder="Senha" required>
                        <select name="tipo_cad_cliente_aprov" style="outline:none" id="tipoCad" required>
                            <option value="Pequeno Produtor">Pequeno Produtor</option>
                            <option value="Médio Produtor">Médio Produtor</option>
                            <option value="Grande Produtor">Grande Produtor</option>
                        </select>
                    </div>

                    <div style="    display: flex;place-content: center;justify-content: space-between;">

                        <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                            Cancelar
                        </button>
                        <button type="submit" class="CadUsuario btn btn-user btn-block">
                            Cadastrar
                        </button>
                    </div>

            </div>
            </form>
        </div>



    </div>
</div>
</div>

<script>
    function abrir_modal_aprovacao(id) {
        $.get("php/get_infos_cliente_aprov.php?id_cliente=" + id, function(data) {
            var json = JSON.parse(data);
            $('#id_cliente_aprov').val(id);
            $('#nome_cliente_aprov').html(json[0].nome);
            $('#email_cliente_aprov').html(json[1].email);
            $('#celular_cliente_aprov').html(json[2].celular);
            $('#cpf_cliente_aprov').html(json[3].cpf);
        });

        $('#modalAprovacaoCliente').modal('show');
    }

    var inputSearch = document.getElementById('pesquisaCliente');
    inputSearch.addEventListener('keyup', function(e) {
        var key = e.which || e.keyCode;
        if (key == 13) {
            $.get("views/subviews/clientes-aprov-pesquisa.php?pesquisa=" + this.value, function(data) {
                $('#div-clientes-aprov').html(data);
            });
        }
    });

    function pesquisa_cliente() {
        var pesquisa = $('#pesquisaCliente').val();
        $.get("views/subviews/clientes-aprov-pesquisa.php?pesquisa=" + pesquisa, function(data) {
            $('#div-clientes-aprov').html(data);
        });
    }
</script>