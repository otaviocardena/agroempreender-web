<?php
include_once("../conn/conexao.php");
session_start();

if (empty($_SESSION['ZWxldHJpY2Ftadm'])) {
  exit(header('Location: login.php'));
} else {
  $id_user = $_SESSION['ZWxldHJpY2Ftadm'];
}

$sql = "SELECT * FROM user_admin WHERE id=$id_user";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
  $username = $row['nome'];
  $avatar = $row['avatar'];
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">

  <title>Agro Empreender | Admin</title>

  <link href="img/logotipo1.png" rel="icon">
  <link href="img/logotipo1.png" rel="apple-touch-icon">

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500;600;700;800;900&display=swap" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">

</head>

<body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <img src="img/logo.png" alt="" style="width:90px">

      </a>
      <hr class="sidebar-divider my-0">


      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link collapsed" style="cursor:pointer" href="index.php">
          <i class="fas fa-home"></i>
          <span>Home</span>
        </a>
      </li>

      <?php if (in_array(1, $_SESSION['permissionsAdm'])) { ?>
        <li class="nav-item">
          <a class="nav-link collapsed " style="cursor:pointer" onclick="page('clientes')">
            <i class="fas fa-user-friends"></i>
            <span>Clientes</span>
          </a>
        </li>

      <?php }
      if (in_array(4, $_SESSION['permissionsAdm'])) { ?>
        <li class="nav-item">
          <a class="nav-link collapsed " style="cursor:pointer" onclick="page('servicos-select')">
            <i class="fas fa-money-bill-wave"></i>
            <span>Serviços</span>
          </a>
        </li>
      <?php }  ?>

      <li class="nav-item">
        <a class="nav-link collapsed " style="cursor:pointer" onclick="page('profile')">
          <i class="fas fa-user"></i>
          <span>Perfil</span>
        </a>
      </li>

      <?php if (in_array(5, $_SESSION['permissionsAdm'])) { ?>
        <li class="nav-item">
          <a class="nav-link collapsed " style="cursor:pointer" onclick="page('usuarios')">
            <i class="fas fa-user-lock"></i>
            <span>Usuários</span>
          </a>
        </li>
      <?php } ?>

      <li class="nav-item">
        <a class="nav-link collapsed " style="cursor:pointer" onclick="page('cadastros-select')">
          <i class="fas fa-user-shield"></i>
          <span>Cadastros</span>
        </a>
      </li>
      <hr class="sidebar-divider">

    </ul>


    <div id="content-wrapper" class="d-flex flex-column">


      <div id="content" style="height: auto;background-attachment: fixed;">


        <nav style="background:#21613A;" class="navbar navbar-expand navbar-light  topbar mb-4 static-top shadow">

          <button id="sidebarToggle" style="background:none; border:none;outline: none;">
            <img src="img/icon_menu.png" style="width:90%;height:90%;">
          </button>
          <div style="width: 100%;text-align-last: center;">
            <div style="color: #fff; font-weight:bold">
              Área Administrativa
            </div>
            <div style="color: #fff;">
              Suas informações administrativas
            </div>
          </div>


          <ul class="navbar-nav ml-auto">



            <div class="topbar-divider d-none d-sm-block"></div>


            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $username ?></span>
                <img class="img-profile rounded-circle" src="data:image/png;base64,<?= $avatar ?>" />
              </a>

              <div style="cursor:pointer;" class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" onclick="page('profile')">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>

                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configurações
                </a>

                <div class="dropdown-divider"></div>

                <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>

              </div>
            </li>

          </ul>

        </nav>


        <div id="conteudo">
        </div>
      </div>


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Realmente deseja sair?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Selecione "Logout" se você está certo em encerrar sua sessão.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="php/logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modalEncerraEtapa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content" style="    border-radius: 30px;">
        <div class="modal-body" style="text-align: -webkit-center;">
          <form action="php/finaliza_etapa.php" method="POST">
            <input type="hidden" id="id_cliente_servico_encerrar" name="id_cliente_servico_encerrar">
            <input type="hidden" id="etapa_encerrar" name="etapa_encerrar">
            <h2 style="text-align-last: center;">Encerrar etapa <span id="num_etapa"></span>?</h2>

            <div style="text-align: -webkit-center;">
              <div style="white-space: pre-line;">
                <textarea name="observacao_encerrar" id="observacao_encerrar" type="text" style="outline:none" class="textCriarCliente" rows="3" maxlength="200" placeholder="Observação"></textarea>
              </div>

              <div style="display: flex;place-content: center;justify-content: space-between;">

                <button type="button" style="margin-top: 5px !important;" class="CancelaUsuario btn btn-user btn-block" data-dismiss="modal" aria-label="Close">
                  Cancelar
                </button>
                <button type="submit" class="CadUsuario btn btn-user btn-block">
                  Finalizar
                </button>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  <script>
    $(document).ready(function() {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      if (window.location.hash.substring(1) != "") {
        var hash = window.location.hash.substring(1);
        window.location.hash = window.location.hash.split(hash)[0];
        console.log(hash);
        $('#conteudo').load("views/" + hash);
      } else {
        $('#conteudo').load("views/home.php");
      }
    });

    function page(pagina) {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      $(document).ready(function() {
        $('#conteudo').load("views/" + pagina + ".php");
      });
    }

    function page_subview(pagina) {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#21613A; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo-subview").html(data);
      $(document).ready(function() {
        $('#conteudo-subview').load("views/subviews/" + pagina + ".php");
      });
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("dot");
      if (n > slides.length) {
        slideIndex = 1
      }
      if (n < 1) {
        slideIndex = slides.length
      }
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex - 1].style.display = "block";
      dots[slideIndex - 1].className += " active";
    }

    function finalizar_etapa(id_cli_serv, etp) {
      $('#id_cliente_servico_encerrar').val(id_cli_serv);
      $('#etapa_encerrar').val(etp);
      $('#num_etapa').html(etp);

      $('#modalEncerraEtapa').modal("show");
    }
  </script>
</body>

</html>