<?php
session_start();

if (!empty($_SESSION['ZWxldHJpY2Ftadm'])) {
  exit(header('Location: index.php'));
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">

  <link href="img/logotipo1.png" rel="icon">
  <link href="img/logotipo1.png" rel="apple-touch-icon">
  
  <title>Agro Empreender</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500;600;700;800;900&display=swap" rel="stylesheet">


  <link href="css/style.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
  <img src="img/FX.png" alt="" style="    position: absolute;width: -webkit-fill-available;height: -webkit-fill-available;">
  <div class="container">

    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="cardLogin card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0" style="    text-align: -webkit-center;">
            <div class="col-lg-6">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="bem-vindo">Acesso Administrativo!</h1>
                  <h1 class="accessar-conta">Acesse sua conta.</h1>
                </div>
                <div>
                  <img src="img/logo.png" alt="" style="width:100%">

                </div>
                <form class="user" id="form-login" action="php/valida.php" style="margin-top: 20px;">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" name="user" aria-describedby="emailHelp" placeholder="Usuário">
                  </div>
                  <div class="form-group" style="display:flex">
                    <input type="password" id="password-field" class="form-control form-control-user" placeholder="Senha" name="pass">
                    <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                  </div>
                  <button onclick="login()" id="loginButton" class="btn btn-user btn-block">
                    Login
                  </button>
                </form>

                <hr>
                <div class="text-center">
                  <i class="fas fa-lock"></i>
                  <a onclick="changeCard()" class="small" id="forgotYPassword">Esqueceu sua senha?</a>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div style="background:#fff;border-radius:2.35rem;" class="cardSolicitacao o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0" style="    text-align: -webkit-center;">
            <div class="col-lg-6">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="bem-vindo">Solicitação de Cadastro!</h1>
                  <h1 class="accessar-conta">Insira suas informações.</h1>
                </div>
                <div>
                  <img src="./img/person-icon.png" alt=""></img>
                </div>
                <form class="user" style="margin-top: 20px;">
                  <div class="form-group">
                    <input class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Nome Completo">
                    <input class="form-control form-control-user" aria-describedby="emailHelp" placeholder="CPF">
                    <input class="form-control form-control-user" aria-describedby="emailHelp" placeholder="E-mail">
                    <input class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Telefone">

                  </div>
                  <a @click="solicitacaoEnviada" id="loginButton" class="btn btn-user btn-block">
                    Enviar
                  </a>

                </form>
                <hr>
                <div class="text-center">
                  <a onclick="voltarLoginbtn()" class="small" id="forgotYPassword">Voltar</a>
                </div>
              </div>
            </div>

          </div>
        </div>


        <div style="border-radius:2.35rem;" class="cardSolicitacaoOk o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0" style="    text-align: -webkit-center;">
            <div class="col-lg-10">
              <div class="p-5">
                <div class="text-center">
                  <img src="./img/ion_checkmark.png" alt=""></img>
                  <h1 style="font-size:3rem" class="bem-vindo">Sucesso!</h1>
                </div>
                <div style="margin-top: 20px;">
                  <h1 class="accessar-conta" style="font-size:20px !important; font-weight:500;">Suas informações foram
                    enviadas com sucesso e estão sob processo de análise. Regresse à tela de login e aguarde seu e-mail
                    de confirmação.</h1>
                  <a style="width: 200px;border-radius: 30px;" @click="voltarMenu2" id="loginButton" class="btn btn-user btn-block">
                    Voltar
                  </a>

                </div>
              </div>
            </div>

          </div>
        </div>



        <div style="background: #21613A;border-radius: 2.35rem;" class="cardEsqueciSenha1 o-hidden border-0 shadow-lg my-5 ">
          <div class="card-body p-0">
            <div class="">
              <!-----esqueceu senha inserir email---->
              <div class="p-5">
                <div class="text-center">
                  <h1 class="esquecer-senha1">Esqueceu sua senha?</h1>
                  <h1 class="esquecer-senha2">Recupere o acesso à sua conta.</h1>
                </div>
                <div style="width:100%; text-align: -webkit-center; margin-top:50px">
                  <div style="display: flex;margin-left: 10px;margin-right: 10px;    text-align: start;     width: 410px;">
                    <i style="font-size: 45px; color: #fff;" class="fas fa-lock"></i>
                    <div style="margin-left: 18px;">
                      <h3 id="aviso1" style="margin-bottom: 0;margin-top: 0;">
                        Insira seu e-mail de acesso.
                      </h3>
                      <h3 id="aviso2" style="margin-bottom: 0;margin-top: 0;">
                        Em instantes iremos enviar um e-mail para recuperação da sua senha.
                      </h3>
                    </div>
                  </div>
                </div>
                <form class="user" style="margin-top: 20px;">
                  <div class="form-group" id="formEsqueciSenha">
                    <input style="font-size:1rem;width:100%;" type="email" class="form-control form-control-user" id="inputEsqueciSenha" aria-describedby="emailHelp" placeholder="E-mail">
                  </div>
                </form>
                <div style=" margin: 0px 26%;">
                  <a onclick="EnviouSenha()" id="enviarSenhabtn" class="btn btn-user btn-block">
                    Enviar
                  </a>
                  <a onclick="voltarLoginbtn()" id="voltarLoginbtn" class="btn btn-user btn-block">
                    <i class="fas fa-angle-double-left"></i>
                    Voltar
                  </a>
                </div>
              </div>
              <!-----esqueceu senha inserir email---->

            </div>
          </div>
        </div>


        <div style="background: #21613A; border-radius: 2.35rem;" class="cardEsqueciSenha2 o-hidden border-0 shadow-lg my-5 ">
          <div class="card-body p-0">
            <div class="">
              <!-----esqueceu senha inserir email---->
              <div class="p-5">
                <div class="text-center">
                  <h1 class="esquecer-senha1">Esqueceu sua senha?</h1>
                  <h1 class="esquecer-senha2">Recupere o acesso à sua conta.</h1>
                </div>
                <div style="width:100%; text-align: -webkit-center;margin-top: 12%;">
                  <div style="display: flex;margin-left: 10px;margin-right: 10px;    text-align: start;     width: 410px;">
                    <i style="    align-self: center;;font-size: 45px; color: #fff;" class="fas fa-lock-open"></i>
                    <div style="margin-left: 18px;">
                      <h3 id="aviso1" style="margin-bottom: 0;margin-top: 0;">
                        Recebemos seu e-mail.
                      </h3>
                      <h3 id="aviso2" style="margin-bottom: 0;margin-top: 0;">
                        Em breve nossa equipe estará entrando em contato para recuperar seu acesso.
                      </h3>
                    </div>
                  </div>
                </div>
                <div style=" margin: 10% 34% 0px 34%;;">
                  <a onclick="voltarLoginbtn()" id="voltarLoginbtn" class="btn btn-user btn-block">
                    Voltar ao menu
                  </a>
                </div>
              </div>
              <!-----esqueceu senha inserir email---->

            </div>
          </div>
        </div>
      </div>
      <div style="text-align: center;">
        <h1 class="dev-by">Developed and Design by</h1>
        <h1 class="dev-evol">EvolutionSoft©</h1>
      </div>

    </div>
  </div>
  </div>




  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/sb-admin-2.min.js"></script>
  <script src="js/functions.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
  <script>
    $(".toggle-password").click(function() {
      $(this).toggleClass("fa-eye-slash fa-eye");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    function login() {
      $('#form-login').submit();
    }

    function changeCard() {
      $(".cardLogin").css("display", "none");
      $(".cardEsqueciSenha1").css("display", "block");
    }

    function voltarLoginbtn() {
      // $(".cardEsqueciSenha1").css("display", "none");
      // $(".cardEsqueciSenha2").css("display", "none");
      // $(".cardSolicitacao").css("display", "none");
      // $(".cardLogin").css("display", "block");
      location.reload();
    }

    function EnviouSenha() {
      if ($("#inputEsqueciSenha").val() != "") {
        $(".cardEsqueciSenha1").css("display", "none");
        $(".cardEsqueciSenha2").css("display", "block");
      } else {
        $("#inputEsqueciSenha").css("border-bottom", "1px solid #d20000");
        $("#inputEsqueciSenha").addClass("error");
        $("#inputEsqueciSenha").focus();
      }
    }
  </script>

</body>

</html>