<?php
include_once("../../conn/conexao.php");

$id_orcamento = $_GET['id'];

$sql = "SELECT * FROM orcamento_cliente_servico_etapa WHERE id = $id_orcamento";
$res = mysqli_query($conn,$sql);

$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('titulo' => $row['titulo']));
	array_push($data,array('valor' => $row['valor']));
	array_push($data,array('cod_banco' => $row['cod_banco']));
	array_push($data,array('agencia' => $row['agencia']));
	array_push($data,array('conta' => $row['conta']));
	array_push($data,array('titular' => $row['titular']));
	array_push($data,array('cnpj' => $row['cnpj']));
	array_push($data,array('motivo' => $row['motivo']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>