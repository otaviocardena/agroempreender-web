<?php
include_once('../../conn/conexao.php');
$id = $_GET['id'];
$tabela = $_GET['tabela'];
// $tabela = "cliente";
// $id = 9;

$sql = "SELECT * FROM documentos_$tabela WHERE id = $id";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $arquivo = $row['documento'];
    $tipo_doc = $row['tipo_doc'];
}

$sql = "SELECT nome FROM tipo_documento WHERE id = $tipo_doc";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)){
    $file = $row[0].".pdf";
}

$decoded = base64_decode($arquivo);
file_put_contents($file, $decoded);

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    unlink($file);
    exit;
}