<?php
include_once("../../conn/conexao.php");

$id = $_GET['id'];

$sql = "SELECT * FROM expansionista WHERE id = $id";
$res = mysqli_query($conn,$sql);

$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('id_planta' => $row['id_planta']));
	array_push($data,array('nome' => $row['nome']));
	array_push($data,array('cpf' => $row['cpf']));
	array_push($data,array('cidade' => $row['cidade']));
	array_push($data,array('endereco' => $row['endereco']));
	array_push($data,array('numero' => $row['numero']));
	array_push($data,array('bairro' => $row['bairro']));
	array_push($data,array('complemento' => $row['complemento']));
	array_push($data,array('estado' => $row['estado']));
	array_push($data,array('setor' => $row['setor']));
}

mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>