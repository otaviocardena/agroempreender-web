<?php
include_once("../../conn/conexao.php");
date_default_timezone_set('America/Sao_Paulo');

function atribui_id($cpf)
{
    if ($cpf != "NULL") {
        global $conn;
        $sql = "SELECT id FROM user_cliente WHERE cpf = '$cpf'";
        $res = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_array($res)) {
            $id_cliente = $row[0];
        }
        return $id_cliente;
    }else{
        return "NULL";
    }
}

$id_servico = $_POST['servico'];
$estado = $_POST['estado'];
$data = date('Y-m-d H:i:s');
$cliente_1 = $_POST['cpf_cliente_1'] ? $_POST['cpf_cliente_1'] : 'NULL';
$cliente_2 = $_POST['cpf_cliente_2'] ? $_POST['cpf_cliente_2'] : 'NULL';
$cliente_3 = $_POST['cpf_cliente_3'] ? $_POST['cpf_cliente_3'] : 'NULL';

$id_cliente_1 = atribui_id($cliente_1);
$id_cliente_2 = atribui_id($cliente_2);
$id_cliente_3 = atribui_id($cliente_3);

$sql = "INSERT INTO cliente_servico(id_cliente,herdeiro_1,herdeiro_2,id_servico,estado,data_cad)
            VALUES($id_cliente_1,$id_cliente_2,$id_cliente_3,$id_servico,'$estado','$data')";
$res = mysqli_query($conn, $sql);

$sql = "SELECT LAST_INSERT_ID() FROM cliente_servico";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_cliente_servico = $row[0];
}

$sql = "SELECT 
            COUNT(es.id) AS num_etapas 
        FROM servicos AS s
        INNER JOIN etapa_servico AS es ON
            es.id_servico = s.id
        WHERE s.id = $id_servico";
$res_servico = mysqli_query($conn, $sql);
while($row = mysqli_fetch_array($res_servico)){
    $etapas = $row[0];
}

for($i = 1; $i <= $etapas; $i++){
    $sql = "INSERT INTO cliente_servico_etapa(id_cliente_servico,etapa) VALUES($id_cliente_servico,$i)";
    $res = mysqli_query($conn, $sql);
}

header("Location: ../index.php#servicos-clientes.php");